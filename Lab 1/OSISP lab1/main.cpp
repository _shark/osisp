#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include "resource.h"
#include <cmath>

// Global variables

// The main window class name.
static TCHAR szWindowClass[] = _T("win32app");

// The string that appears in the application's title bar.
static TCHAR szTitle[] = _T("Lab1");
static HACCEL hAccel;

HINSTANCE hInst;

// Forward declarations of functions included in this code module:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground  = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wcex.lpszMenuName   = MAKEINTRESOURCE(IDM_MENU);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

    if (!RegisterClassEx(&wcex))
    {
        MessageBox(NULL,
            _T("Call to RegisterClassEx failed!"),
            _T("Lab1"),
            NULL);

        return 1;
    }

    hInst = hInstance; // Store instance handle in our global variable

    // The parameters to CreateWindow explained:
    // szWindowClass: the name of the application
    // szTitle: the text that appears in the title bar
    // WS_OVERLAPPEDWINDOW: the type of window to create
    // CW_USEDEFAULT, CW_USEDEFAULT: initial position (x, y)
    // 500, 100: initial size (width, length)
    // NULL: the parent of this window
    // NULL: this application does not have a menu bar
    // hInstance: the first parameter from WinMain
    // NULL: not used in this application
    HWND hWnd = CreateWindow(
        szWindowClass,
        szTitle,
		WS_OVERLAPPEDWINDOW| WS_CLIPCHILDREN,
        CW_USEDEFAULT, CW_USEDEFAULT,
        640, 480,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    if (!hWnd)
    {
        MessageBox(NULL,
            _T("Call to CreateWindow failed!"),
            _T("Win32 Guided Tour"),
            NULL);

        return 1;
    }

    // The parameters to ShowWindow explained:
    // hWnd: the value returned from CreateWindow
    // nCmdShow: the fourth parameter from WinMain
    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    // Main message loop:
    MSG msg;
	hAccel = LoadAccelerators(hInst, MAKEINTRESOURCE(MAKEINTRESOURCE(IDR_ACCELERATOR1)));
    while (GetMessage(&msg, NULL, 0, 0))
    {
		TranslateAccelerator(hWnd, hAccel, &msg);
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int) msg.wParam;
}


void ChangeSize(HWND hWnd, HBITMAP *hBitMap1, HDC hMemDC1, HANDLE *oldHandle1)
{
	HBITMAP hTempBitMap;
	BITMAP bmpinfo;
	HDC hTempDC, hdc;
	HANDLE hTemp;
	RECT rect;

	hdc = GetDC(hWnd);
	GetObject(*hBitMap1,sizeof(BITMAP),(LPSTR)&bmpinfo);
	hTempDC = CreateCompatibleDC(hdc);
	hTempBitMap = CreateCompatibleBitmap(hdc, bmpinfo.bmWidth, bmpinfo.bmHeight);
	hTemp = SelectObject(hTempDC, hTempBitMap);
	BitBlt(hTempDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, hMemDC1, 0, 0, SRCCOPY);
	SelectObject(hMemDC1, oldHandle1);
	DeleteObject(hBitMap1);
	GetClientRect(hWnd, &rect);
	*hBitMap1 = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
	*oldHandle1 = SelectObject(hMemDC1, *hBitMap1);
	PatBlt(hMemDC1, 0, 0, rect.right, rect.bottom, WHITENESS);
	BitBlt(hMemDC1, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, hTempDC, 0, 0, SRCCOPY);
	SelectObject(hTempDC, hTemp);
	DeleteDC(hTempDC);
	DeleteObject(hTempBitMap);
	ReleaseDC(hWnd, hdc);
}

BOOL CALLBACK AboutDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam) 
     { 
     switch(iMsg) 
          { 
          case WM_INITDIALOG : 
               return TRUE; 
 
          case WM_COMMAND : 
               switch(LOWORD(wParam)) 
                    { 
                    case IDOK : 
						EndDialog(hDlg, 0); 
						return TRUE;
                    } 
               break; 
          } 
     return FALSE; 
     }

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;
    TCHAR greeting[] = _T("Hello, World!");
	RECT rect;
	static HDC hMemDC1, hMemDC2;
	static HBITMAP hBitMap1, hBitMap2;
	static int startX, startY, current_shape = IDM_PEN, mouse_clicked = 0,
	startPolygonX = -1, startPolygonY = -1, current_width = 1, endX, endY,
	offsetX = 0, offsetY = 0;
	static HANDLE oldHandle1, oldHandle2;
	TCHAR charCode;
	static TCHAR textStr[100];
	static CHOOSECOLOR cc1 = {0}, cc2 = {0};
	static COLORREF cust_colors[16] = { 0 };
	static double scale = 1;
	double wheel_delta;
	OPENFILENAME ofn;
	static TCHAR fullpath[256], filename[256];
	HENHMETAFILE hemf;
	HDC hdcEMF;
	PRINTDLG pd;

    switch (message)
    {
	case WM_CREATE:

		hdc = GetDC(hWnd);

		GetClientRect(hWnd, &rect);
		hMemDC1 = CreateCompatibleDC(hdc);
		hBitMap1 = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);

		oldHandle1 = SelectObject(hMemDC1, hBitMap1);
		PatBlt(hMemDC1, 0, 0, rect.right, rect.bottom, WHITENESS); 
		
		hMemDC2 = CreateCompatibleDC(hdc);
		hBitMap2 = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);

		oldHandle2 = SelectObject(hMemDC2, hBitMap2);
		PatBlt(hMemDC2, 0, 0, rect.right, rect.bottom, WHITENESS);
		textStr[0] = '\0';

		cc1.lStructSize = sizeof(CHOOSECOLOR);
		cc1.hInstance = NULL;
		cc1.hwndOwner = hWnd;
		cc1.lpCustColors = cust_colors;
		cc1.Flags = CC_RGBINIT|CC_FULLOPEN;
		cc1.lCustData = 0L;
		cc1.lpfnHook = NULL;
		cc1.rgbResult = RGB(0, 0, 0);
		cc1.lpTemplateName = NULL;

		cc2.lStructSize = sizeof(CHOOSECOLOR);
		cc2.hInstance = NULL;
		cc2.hwndOwner = hWnd;
		cc2.lpCustColors = cust_colors;
		cc2.Flags = CC_RGBINIT|CC_FULLOPEN;
		cc2.lCustData = 0L;
		cc2.lpfnHook = NULL;
		cc2.rgbResult = RGB(0, 0, 0);
		cc2.lpTemplateName = NULL;

		ReleaseDC(hWnd, hdc);

		hdcEMF = CreateEnhMetaFile ( NULL, L"sd.emf", NULL, NULL ) ; 
        //SelectObject(hdcEMF,CreateSolidBrush(RGB(rand()%255,rand()%255,rand()%255)));
		PatBlt(hdcEMF, 0, 0, rect.right, rect.bottom, WHITENESS);
        Rectangle(hdcEMF, 0, 0, 100, 100);
        DeleteEnhMetaFile(CloseEnhMetaFile ( hdcEMF ));

		break;
	case WM_COMMAND:
		if(lParam == 0)
		{
			switch (LOWORD(wParam))
			{
			case IDM_EXIT:
				SendMessage(hWnd, WM_CLOSE, 0, 0L);
				return 0;
			case IDM_PEN:
				current_shape = IDM_PEN;
				break;
			case IDM_LINE:
				current_shape = IDM_LINE;
				break;
			case IDM_POLYLINE:
				current_shape = IDM_POLYLINE; 
				break;
			case IDM_RECTANGLE:
				current_shape = IDM_RECTANGLE;
				break;
			case IDM_POLYGON:
				current_shape = IDM_POLYGON;
				break;
			case IDM_ELLIPSE:
				current_shape = IDM_ELLIPSE;
				break;
			case IDM_TEXT:
				current_shape = IDM_TEXT;
				break;
			case ID_PENWIDTH_1:
				current_width = 1;
				DeleteObject(SelectObject(hMemDC1, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				DeleteObject(SelectObject(hMemDC2, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				break;
			case ID_PENWIDTH_2:
				current_width = 2;
				DeleteObject(SelectObject(hMemDC1, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				DeleteObject(SelectObject(hMemDC2, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				break;
			case ID_PENWIDTH_3:
				current_width = 3;
				DeleteObject(SelectObject(hMemDC1, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				DeleteObject(SelectObject(hMemDC2, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));				
				break;
			case ID_PENWIDTH_4:
				current_width = 4;
				DeleteObject(SelectObject(hMemDC1, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				DeleteObject(SelectObject(hMemDC2, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				break;
			case ID_COLORS_PENCOLOR:
				if(ChooseColor(&cc1))
				{
					DeleteObject(SelectObject(hMemDC1, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
					DeleteObject(SelectObject(hMemDC2, CreatePen(PS_SOLID, current_width, cc1.rgbResult)));
				}
				break;
			case ID_COLORS_BRUSHCOLOR:
				if(ChooseColor(&cc2))
				{
					DeleteObject(SelectObject(hMemDC1, CreateSolidBrush(cc2.rgbResult)));
					DeleteObject(SelectObject(hMemDC2, CreateSolidBrush(cc2.rgbResult)));
				}
				break;
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, AboutDlgProc);
				break;
			case IDM_OPEN:
				ZeroMemory(&ofn, sizeof(OPENFILENAME));
				ofn.lStructSize=sizeof(OPENFILENAME);
				ofn.hwndOwner=hWnd;
				ofn.hInstance=hInst;
				ofn.lpstrFilter=L"Enhanced Metafile (*.emf)\0*.emf\0";
				ofn.nFilterIndex=1;
				ofn.lpstrFile = fullpath;
				ofn.nMaxFile=sizeof(fullpath);
				ofn.lpstrFileTitle=NULL;
				ofn.nMaxFileTitle=0;
				ofn.lpstrInitialDir = NULL;
				ofn.lpstrTitle=L"Open file";
				ofn.Flags=OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_HIDEREADONLY|OFN_EXPLORER;

				if(GetOpenFileName(&ofn))
				{
					GetClientRect(hWnd, &rect);
					hemf = GetEnhMetaFile(ofn.lpstrFile);
					PlayEnhMetaFile(hMemDC1, hemf, &rect);
					PlayEnhMetaFile(hMemDC2, hemf, &rect);
					DeleteEnhMetaFile(hemf);
					InvalidateRect(hWnd, NULL, false);
					UpdateWindow(hWnd);
				}
				break;
			case IDM_SAVE:
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize=sizeof(ofn);
				ofn.hwndOwner=hWnd;
				ofn.hInstance=hInst; // ���������� ����� ����������
				ofn.lpstrFilter=L"Enhanced Metafile (*.emf)\0*.emf\0";
				ofn.nFilterIndex=1;
				ofn.lpstrFile = fullpath;
				ofn.lpstrFile[0] = '\0';
				ofn.nMaxFile=sizeof(fullpath);
				ofn.lpstrFileTitle=filename;
				ofn.nMaxFileTitle=sizeof(filename);
				ofn.lpstrInitialDir = NULL;
				ofn.lpstrTitle=L"Save file as...";
				ofn.Flags=OFN_CREATEPROMPT|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY|OFN_EXPLORER;

				if(GetSaveFileName(&ofn))
				{
					GetClientRect(hWnd, &rect);
					hdcEMF = CreateEnhMetaFile(NULL, ofn.lpstrFile, NULL, NULL);
					StretchBlt(hdcEMF, 0, 0, rect.right, rect.bottom, hMemDC1, -offsetX, -offsetY, (int)rect.right*scale, (int)rect.bottom*scale, SRCCOPY);
					DeleteEnhMetaFile(CloseEnhMetaFile(hdcEMF));
				}
				break;
			case IDM_PRINT:
				ZeroMemory(&pd, sizeof(pd));
				pd.lStructSize = sizeof(pd);
				pd.hwndOwner   = hWnd;
				pd.hDevMode    = NULL; 
				pd.hDevNames   = NULL; 
				pd.Flags       = PD_USEDEVMODECOPIESANDCOLLATE | PD_RETURNDC;
				pd.nCopies     = 1;
				pd.nFromPage   = 1;
				pd.nToPage     = 1;
				pd.nMinPage    = 1;
				pd.nMaxPage    = 1;

				if(PrintDlg(&pd))
				{
					int printDPIX = GetDeviceCaps(pd.hDC, LOGPIXELSX);
					int printDPIY = GetDeviceCaps(pd.hDC, LOGPIXELSY);
					int printMMWidth = GetDeviceCaps(pd.hDC, HORZSIZE);
					int printMMHeight = GetDeviceCaps(pd.hDC, VERTSIZE);

					DOCINFO di;
					di.cbSize=sizeof(DOCINFO);
					di.lpszDocName = L"Printing data";
					di.fwType = NULL; 
					di.lpszDatatype = NULL;
					di.lpszOutput = NULL;
					
					StartDoc(pd.hDC,&di);
					StartPage(pd.hDC);

					GetClientRect(hWnd,&rect);
					
					int canvasDPIX = GetDeviceCaps(hMemDC1, LOGPIXELSX);
					int canvasDPIY = GetDeviceCaps(hMemDC1, LOGPIXELSY);

					StretchBlt(pd.hDC, 0, 0, (int)((printDPIX*printMMWidth)*0.0393), (int)((printDPIY*printMMHeight)*0.0393),
					hMemDC1, 0, 0, rect.right, rect.bottom, SRCCOPY);

					EndPage(pd.hDC);
					EndDoc(pd.hDC);

					DeleteDC(pd.hDC);
				}

				break;
			}
		}
		break;
	case WM_KEYDOWN:
		GetClientRect(hWnd, &rect);
		switch (wParam) 
		{
			case VK_LEFT:
				if(offsetX + 5 < 0)
					offsetX += 5;
				else
					offsetX += 1;
				if(offsetX > 0)
					offsetX = 0;
				InvalidateRect(hWnd, NULL, true);
				UpdateWindow(hWnd);
			break;
			case VK_RIGHT:
				if(offsetX - 5> rect.right*scale - rect.right)
					offsetX -= 5;
				else
					offsetX -= 1;
				if(offsetX < rect.right*scale - rect.right)
					offsetX = rect.right*scale - rect.right;
				InvalidateRect(hWnd, NULL, true);
				UpdateWindow(hWnd);
			break;
			case VK_UP:
				if(offsetY +5 < 0)
					offsetY += 5;
				else
					offsetY += 1;
				if(offsetY > 0)
					offsetY = 0;
				InvalidateRect(hWnd, NULL, true);
				UpdateWindow(hWnd);
			break;
			case VK_DOWN:
				if(offsetY -5 > rect.bottom*scale - rect.bottom)
					offsetY -= 5;
				else
					offsetY -= 1;
				if(offsetY < rect.bottom*scale - rect.bottom)
					offsetY = rect.bottom*scale - rect.bottom;
				InvalidateRect(hWnd, NULL, true);
				UpdateWindow(hWnd);
			break;
		}
		break;
	case WM_MOUSEWHEEL:
		GetClientRect(hWnd, &rect);
		wheel_delta = GET_WHEEL_DELTA_WPARAM(wParam);
		if((wheel_delta<0)&&(scale<2))
		   scale +=0.05;
		if((wheel_delta>0)&&(scale>0.2))
			scale=scale-0.05;
		offsetX = (rect.right*scale - rect.right)/2;
		offsetY = (rect.bottom*scale - rect.bottom)/2;
		InvalidateRect(hWnd,NULL,true);
		UpdateWindow(hWnd);
		break;
	case WM_SIZE:
		ChangeSize(hWnd, &hBitMap1, hMemDC1, &oldHandle1);
		ChangeSize(hWnd, &hBitMap2, hMemDC2, &oldHandle2);
		break;
	case WM_ERASEBKGND:
		if(scale > 1)
		{
			hdc = GetDC(hWnd);
			GetClientRect(hWnd, &rect);
			RECT r;
			r.left = rect.right/scale;
			r.right = rect.right;
			r.top = 0;
			r.bottom = rect.bottom;
			FillRect(hdc, &r, (HBRUSH)LTGRAY_BRUSH);
			r.left = 0;
			r.right = rect.right;
			r.top = rect.bottom/scale;
			r.bottom = rect.bottom;
			FillRect(hdc, &r, (HBRUSH)LTGRAY_BRUSH);
			ReleaseDC(hWnd, hdc);
		}
		break;
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
		GetClientRect(hWnd, &rect);
		
		if(scale > 1)
			offsetX = offsetY = 0;
		StretchBlt(hdc, 0, 0, rect.right, rect.bottom, hMemDC2, -offsetX, -offsetY, (int)rect.right*scale, (int)rect.bottom*scale, SRCCOPY);
        EndPaint(hWnd, &ps);
        break;

	case WM_CHAR:
		if(current_shape == IDM_TEXT)
		{
			GetClientRect(hWnd, &rect);
			PatBlt(hMemDC2, 0, 0, rect.right, rect.bottom, WHITENESS);
			BitBlt(hMemDC2, 0, 0, rect.right, rect.bottom, hMemDC1, 0, 0, SRCCOPY);
			charCode = (TCHAR) wParam;
			if(charCode == VK_RETURN)
			{
				TextOut(hMemDC2, startX, startY, textStr, _tcslen(textStr));
				BitBlt(hMemDC1, 0, 0, rect.right, rect.bottom, hMemDC2, 0, 0, SRCCOPY);
				memset(textStr, 0, 100);
			}
			if(charCode == VK_BACK)
			{
				textStr[_tcslen(textStr) - 1] = '\0';
			}
			else
				textStr[_tcslen(textStr)] = charCode;
			TextOut(hMemDC2, startX, startY, textStr, _tcslen(textStr));
			InvalidateRect(hWnd, NULL, true);
			UpdateWindow(hWnd);
		}
		break;
	case WM_LBUTTONDOWN:
		mouse_clicked = 1;
		startX = LOWORD(lParam)*scale - offsetX;
		startY = HIWORD(lParam)*scale - offsetY;
		if(current_shape == IDM_POLYLINE || current_shape == IDM_POLYGON)
		{
			if(current_shape == IDM_POLYGON && startPolygonX == -1)
			{
				startPolygonX = startX;
				startPolygonY = startY;
			}
			GetClientRect(hWnd, &rect);
			BitBlt(hMemDC1, 0, 0, rect.right, rect.bottom, hMemDC2, 0, 0, SRCCOPY);
		}
		if(current_shape == IDM_TEXT)
		{
			GetClientRect(hWnd, &rect);
			PatBlt(hMemDC2, 0, 0, rect.right, rect.bottom, WHITENESS);
			BitBlt(hMemDC2, 0, 0, rect.right, rect.bottom, hMemDC1, 0, 0, SRCCOPY);
			memset(textStr, 0, 100);
			InvalidateRect(hWnd, NULL, true);
			UpdateWindow(hWnd);
		}
		break;
	case WM_RBUTTONDOWN:
		if(current_shape == IDM_POLYLINE || current_shape == IDM_POLYGON)
		{
			if(current_shape == IDM_POLYGON && startPolygonX !=-1)
			{
				LineTo(hMemDC2, startPolygonX, startPolygonY);
				startPolygonX = -1;
				startPolygonY = -1;
			}
			mouse_clicked = 0;
			GetClientRect(hWnd, &rect);
			BitBlt(hMemDC1, 0, 0, rect.right, rect.bottom, hMemDC2, 0, 0, SRCCOPY);
			InvalidateRect(hWnd, NULL, false);
			UpdateWindow(hWnd);
		}
		break;
	case WM_MOUSEMOVE:
		if(mouse_clicked)
		{
			endX = LOWORD(lParam)*scale - offsetX;
			endY = HIWORD(lParam)*scale - offsetY;
			GetClientRect(hWnd, &rect);
			PatBlt(hMemDC2, 0, 0, rect.right, rect.bottom, WHITENESS);
			BitBlt(hMemDC2, 0, 0, rect.right, rect.bottom, hMemDC1, 0, 0, SRCCOPY);
			switch (current_shape)
			{
				case IDM_PEN:
					MoveToEx(hMemDC1, startX, startY, NULL);
					LineTo(hMemDC1, endX, endY);
					MoveToEx(hMemDC2, startX, startY, NULL);
					LineTo(hMemDC2, endX, endY);
					startX = LOWORD(lParam)*scale - offsetX;
					startY = HIWORD(lParam)*scale - offsetY;
					break;
				case IDM_LINE:
				case IDM_POLYGON:
				case IDM_POLYLINE:
					MoveToEx(hMemDC2, startX, startY, NULL);
					LineTo(hMemDC2, endX, endY);
					break;
				case IDM_RECTANGLE:
					Rectangle(hMemDC2, startX, startY, endX, endY);
					break;
				case IDM_ELLIPSE:
					Ellipse(hMemDC2, startX, startY, endX, endY);
					break;
			}
			InvalidateRect(hWnd, NULL, false);
			UpdateWindow(hWnd);
		}
		break;
	case WM_LBUTTONUP:
		if(current_shape != IDM_POLYLINE && current_shape != IDM_POLYGON)
			mouse_clicked = 0;
		switch (current_shape)
		{
			case IDM_PEN:
				endX = LOWORD(lParam)*scale - offsetX;
				endY = HIWORD(lParam)*scale - offsetY;
				SetPixel(hMemDC1, endX, endY, 0);
				SetPixel(hMemDC2, endX, endY, 0);
				break;
			case IDM_LINE:
			case IDM_RECTANGLE:
			case IDM_ELLIPSE:
				GetClientRect(hWnd, &rect);
				BitBlt(hMemDC1, 0, 0, rect.right, rect.bottom, hMemDC2, 0, 0, SRCCOPY);
				break;
			case IDM_TEXT:
				
				break;
		}
		InvalidateRect(hWnd, NULL, false);
		UpdateWindow(hWnd);
		break;
    case WM_DESTROY:
		SelectObject(hMemDC1, oldHandle1);
		DeleteObject(hBitMap1);
		DeleteDC(hMemDC1);
		SelectObject(hMemDC2, oldHandle2);
		DeleteObject(hBitMap2);
		DeleteDC(hMemDC2);
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
        break;
    }

    return 0;
}