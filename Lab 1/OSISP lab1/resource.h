//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется OSISP lab1.rc
//
#define IDM_OPEN                        10
#define IDM_SAVE                        11
#define IDM_PRINT                       12
#define IDM_EXIT                        13
#define IDM_PEN                         20
#define IDM_LINE                        21
#define IDM_POLYLINE                    22
#define IDM_RECTANGLE                   23
#define IDM_POLYGON                     24
#define IDM_ELLIPSE                     25
#define IDM_TEXT                        26
#define IDM_ABOUT                       40
#define IDM_MENU                        101
#define IDD_DIALOG1                     102
#define IDB_BITMAP2                     106
#define IDI_ICON1                       107
#define IDR_ACCELERATOR1                108
#define ID_PENWIDTH                     40013
#define ID_COLORS                       40014
#define ID_COLORS_PENCOLOR              40015
#define ID_COLORS_BRUSHCOLOR            40016
#define ID_PENWIDTH_1                   40017
#define ID_PENWIDTH_2                   40018
#define ID_PENWIDTH_3                   40019
#define ID_PENWIDTH_4                   40020
#define ID_COLORS_TEXTCOLOR             40021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40026
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
