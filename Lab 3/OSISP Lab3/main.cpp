#include <Windows.h>
#include <iostream>
#include <string>

#define SHARED_MEMORY_SIZE 256
#define SHARED_MEMORY_NAME TEXT("SharedMem")
#define THREAD_COUNT 2

int *pSharedValue = NULL;

DWORD WINAPI inc(LPVOID);
HANDLE hMutex = NULL;

int main()
{
	HANDLE hSharedMemory;
	HANDLE hThreads[THREAD_COUNT];

	if((hSharedMemory=CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, SHARED_MEMORY_SIZE, SHARED_MEMORY_NAME)) == NULL)
    {
        std::cout << "Error to Mapping file" << std::endl;
        return -1;
    }

	if(GetLastError() == ERROR_ALREADY_EXISTS)
	{
		pSharedValue = (int *)MapViewOfFile(hSharedMemory, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(int));
	}
	else
	{
		pSharedValue = (int *)MapViewOfFile(hSharedMemory, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(int));
		*pSharedValue = 0;
	}

	hMutex = CreateMutex(NULL, false, TEXT("SharedMutex"));


	for(int i = 0; i < THREAD_COUNT; i++)
	{
		hThreads[i] = CreateThread(NULL, NULL, inc, NULL, NULL, NULL);
	}
	
	for(int i = 0; i < THREAD_COUNT; i++)
	{
		WaitForSingleObject(hThreads[i], INFINITE);
		CloseHandle(hThreads[i]);
	}

	UnmapViewOfFile(pSharedValue);

	CloseHandle(hSharedMemory);
	CloseHandle(hMutex);

	system("pause");

	return 0;
}

DWORD WINAPI inc(LPVOID param)
{
	std::string info = "";

	for(int i = 0; i < 5; i++)
	{
		info = "";

		WaitForSingleObject(hMutex, INFINITE);

		(*pSharedValue)++;

		info += "Process ";
		info += std::to_string(GetCurrentProcessId());
		info += "\nThread ";
		info += std::to_string(GetCurrentThreadId());
		info += "\nNew value  = ";
		info += std::to_string(*pSharedValue);
		info += "\n\n";

		std::cout << info;
		Sleep(1000);

		ReleaseMutex(hMutex);
	}

	return 0;
}