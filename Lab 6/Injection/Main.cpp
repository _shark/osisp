#include <Windows.h>
#include <stdio.h>


//LPWSTR lpProcName = L"C:\\Program Files (x86)\\Notepad++\\notepad++.exe";
//LPWSTR lpProcName = L"C:\\Program Files (x86)\\STDU Viewer\\STDUViewerApp.exe";
//LPWSTR lpProcName = L"C:\\Program Files\\paint.net\\PaintDotNet.exe";
LPWSTR lpProcName = L"C:\\Users\\Stanislav\\Desktop\\�����\\��� Lab2\\UnitTestProject1\\bin\\Debug\\��� Lab2.exe";
//LPWSTR lpProcName = L"C:\\Windows\\System32\\notepad.exe";

static WCHAR wcParam[] = L"C:\\Users\\Stanislav\\Desktop\\�����\\OSISP part 2\\Lab 6\\Debug\\Lab 6.dll";	

int main()
{
	DWORD dwFlags = CREATE_DEFAULT_ERROR_MODE | CREATE_SUSPENDED;
	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));
    si.cb = sizeof(si);

	if(CreateProcessW(lpProcName, NULL, NULL, NULL, false, dwFlags, NULL, NULL, &si, &pi) == 0)
	{
		DWORD dwError = GetLastError();
        printf("CreateProcess failed: %d\n", dwError);
		getchar();
		return -1;
	}

	int cch = 1 + lstrlenW(wcParam);
    int cb  = cch * sizeof(WCHAR);

	// Allocate space in the remote process for the pathname
	PWSTR pszLibFileRemote = (PWSTR) VirtualAllocEx(pi.hProcess, NULL, cb, MEM_COMMIT, PAGE_READWRITE);
	if (pszLibFileRemote == NULL)
	{
		printf("Allocate space in remote process failed");
		getchar();
		return -1;
	}

	// Copy the DLL's pathname to the remote process's address space
	if (!WriteProcessMemory(pi.hProcess, pszLibFileRemote,(PVOID) wcParam, cb, NULL))
	{
		printf("Write to memory failed");
		getchar();
		return -1;
	}

	// Get the real address of LoadLibraryW in Kernel32.dll
	PTHREAD_START_ROUTINE pfnThreadRtn = (PTHREAD_START_ROUTINE) GetProcAddress(GetModuleHandle(TEXT("Kernel32")), "LoadLibraryW");
	if (pfnThreadRtn == NULL)
	{
		printf("GetProcAddress failed");
		getchar();
		return -1;
	}

	// Create a remote thread that calls LoadLibraryW(DLLPathname)
	HANDLE hThread = CreateRemoteThread(pi.hProcess, NULL, 0,	pfnThreadRtn, pszLibFileRemote, 0, NULL);
	if (hThread == NULL)
	{
		DWORD dwError = GetLastError();
        printf("CreateRemoteThread failed: %d\n", dwError);
		getchar();
		return -1;
	}

	ResumeThread(pi.hThread);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	// Wait for the remote thread to terminate
	WaitForSingleObject(hThread, INFINITE);

	// Free the remote memory that contained the DLL's pathname
	if (pszLibFileRemote != NULL) 
		VirtualFreeEx(pi.hProcess, pszLibFileRemote, 0, MEM_RELEASE);

	if (hThread  != NULL) 
		CloseHandle(hThread);

	return 0;
}