// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include <Windows.h>
#include <winefs.h>
#include <string>
#include <fstream>
#include <sstream>
#include <detours.h>

#pragma comment(lib, "detours.lib")

std::ofstream logfile;
std::stringstream logdata;

void WriteToLogFile(std::string data)
{
	logdata << data;
	logdata << "\n";
}

void EndLogging()
{
	logfile << logdata.rdbuf();
	logfile.flush();
	logfile.close();
}

//------------File Management functions start------------//
BOOL(WINAPI *rCancelIo)(HANDLE) = CancelIo;
BOOL(WINAPI *rCancelIoEx)(HANDLE, LPOVERLAPPED) = CancelIoEx;
BOOL(WINAPI *rCancelSynchronousIo)(HANDLE) = CancelSynchronousIo;
HANDLE(WINAPI *rCreateIoCompletionPort)(HANDLE, HANDLE, ULONG_PTR, DWORD) = CreateIoCompletionPort;
BOOL(WINAPI *rFlushFileBuffers)(HANDLE) = FlushFileBuffers;
BOOL(WINAPI *rGetQueuedCompletionStatus)(HANDLE, LPDWORD, PULONG_PTR, LPOVERLAPPED*, DWORD) = GetQueuedCompletionStatus;
BOOL(WINAPI *rGetQueuedCompletionStatusEx)(HANDLE, LPOVERLAPPED_ENTRY, ULONG, PULONG, DWORD, BOOL) = GetQueuedCompletionStatusEx;
BOOL(WINAPI *rLockFile)(HANDLE, DWORD, DWORD, DWORD, DWORD) = LockFile;
BOOL(WINAPI *rLockFileEx)(HANDLE, DWORD, DWORD, DWORD, DWORD, LPOVERLAPPED) = LockFileEx;
BOOL(WINAPI *rPostQueuedCompletionStatus)(HANDLE, DWORD, ULONG_PTR, LPOVERLAPPED) = PostQueuedCompletionStatus;
BOOL(WINAPI *rReadFile)(HANDLE, LPVOID, DWORD, LPDWORD, LPOVERLAPPED) = ReadFile;
BOOL(WINAPI *rReadFileEx)(HANDLE, LPVOID, DWORD, LPOVERLAPPED, LPOVERLAPPED_COMPLETION_ROUTINE) = ReadFileEx;
BOOL(WINAPI *rReadFileScatter)(HANDLE, FILE_SEGMENT_ELEMENT*, DWORD, LPDWORD, LPOVERLAPPED) = ReadFileScatter;
BOOL(WINAPI *rSetEndOfFile)(HANDLE) = SetEndOfFile;
BOOL(WINAPI *rSetFileCompletionNotificationModes)(HANDLE, UCHAR) = SetFileCompletionNotificationModes;
BOOL(WINAPI *rSetFileIoOverlappedRange)(HANDLE, PUCHAR, ULONG) = SetFileIoOverlappedRange;
DWORD(WINAPI *rSetFilePointer)(HANDLE, LONG, PLONG, DWORD) = SetFilePointer;
BOOL(WINAPI *rSetFilePointerEx)(HANDLE, LARGE_INTEGER, PLARGE_INTEGER, DWORD) = SetFilePointerEx;
BOOL(WINAPI *rUnlockFile)(HANDLE, DWORD, DWORD, DWORD, DWORD) = UnlockFile;
BOOL(WINAPI *rUnlockFileEx)(HANDLE, DWORD, DWORD, DWORD, LPOVERLAPPED) = UnlockFileEx;
BOOL(WINAPI *rWriteFile)(HANDLE, LPCVOID, DWORD, LPDWORD, LPOVERLAPPED) = WriteFile;
BOOL(WINAPI *rWriteFileEx)(HANDLE, LPCVOID, DWORD, LPOVERLAPPED, LPOVERLAPPED_COMPLETION_ROUTINE) = WriteFileEx;
BOOL(WINAPI *rWriteFileGather)(HANDLE, FILE_SEGMENT_ELEMENT*, DWORD, LPDWORD, LPOVERLAPPED) = WriteFileGather;
DWORD(WINAPI *rAddUsersToEncryptedFile)(LPCWSTR, PENCRYPTION_CERTIFICATE_LIST) = AddUsersToEncryptedFile;
void(WINAPI *rCloseEncryptedFileRaw)(PVOID) = CloseEncryptedFileRaw;
BOOL(WINAPI *rDecryptFile)(LPCTSTR, DWORD) = DecryptFile;
DWORD(WINAPI *rDuplicateEncryptionInfoFile)(LPCTSTR, LPCTSTR, DWORD, DWORD, LPSECURITY_ATTRIBUTES) = DuplicateEncryptionInfoFile;
BOOL(WINAPI *rEncryptFile)(LPCTSTR) = EncryptFile;
BOOL(WINAPI *rEncryptionDisable)(LPCWSTR, BOOL) = EncryptionDisable;
BOOL(WINAPI *rFileEncryptionStatus)(LPCTSTR, LPDWORD) = FileEncryptionStatus;
void(WINAPI *rFreeEncryptionCertificateHashList)(PENCRYPTION_CERTIFICATE_HASH_LIST) = FreeEncryptionCertificateHashList;
DWORD(WINAPI *rOpenEncryptedFileRaw)(LPCTSTR, ULONG, PVOID*) = OpenEncryptedFileRaw;
DWORD(WINAPI *rQueryRecoveryAgentsOnEncryptedFile)(LPCWSTR, PENCRYPTION_CERTIFICATE_HASH_LIST*) = QueryRecoveryAgentsOnEncryptedFile;
DWORD(WINAPI *rQueryUsersOnEncryptedFile)(LPCWSTR, PENCRYPTION_CERTIFICATE_HASH_LIST*) = QueryUsersOnEncryptedFile;
DWORD(WINAPI *rReadEncryptedFileRaw)(PFE_EXPORT_FUNC, PVOID, PVOID) = ReadEncryptedFileRaw;
DWORD(WINAPI *rRemoveUsersFromEncryptedFile)(LPCWSTR, PENCRYPTION_CERTIFICATE_HASH_LIST) = RemoveUsersFromEncryptedFile;
DWORD(WINAPI *rWriteEncryptedFileRaw)(PFE_IMPORT_FUNC, PVOID, PVOID) = WriteEncryptedFileRaw;
DWORD(WINAPI *rSetUserFileEncryptionKey)(PENCRYPTION_CERTIFICATE) = SetUserFileEncryptionKey;
BOOL(WINAPI *rAreFileApisANSI)(void) = AreFileApisANSI;
BOOL(WINAPI *rCheckNameLegalDOS8Dot3)(LPCTSTR, LPSTR, DWORD, PBOOL, PBOOL) = CheckNameLegalDOS8Dot3;
BOOL(WINAPI *rCopyFile)(LPCTSTR, LPCTSTR, BOOL) = CopyFile;
BOOL(WINAPI *rCopyFileEx)(LPCTSTR, LPCTSTR, LPPROGRESS_ROUTINE, LPVOID, LPBOOL, DWORD) = CopyFileEx;
BOOL(WINAPI *rCopyFileTransacted)(LPCTSTR, LPCTSTR, LPPROGRESS_ROUTINE, LPVOID, LPBOOL, DWORD, HANDLE) = CopyFileTransacted;
HANDLE(WINAPI *rCreateFile)(LPCTSTR, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE) = CreateFile;
HANDLE(WINAPI *rCreateFileTransacted)(LPCTSTR, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE, HANDLE, PUSHORT, PVOID) = CreateFileTransacted;
BOOL(WINAPI *rCreateHardLink)(LPCTSTR, LPCTSTR, LPSECURITY_ATTRIBUTES) = CreateHardLink;
BOOL(WINAPI *rCreateHardLinkTransacted)(LPCTSTR, LPCTSTR, LPSECURITY_ATTRIBUTES, HANDLE) = CreateHardLinkTransacted;
BOOLEAN(WINAPI *rCreateSymbolicLink)(LPCWSTR, LPCWSTR, DWORD) = CreateSymbolicLink;
BOOLEAN(WINAPI *rCreateSymbolicLinkTransacted)(LPCWSTR, LPCWSTR, DWORD, HANDLE) = CreateSymbolicLinkTransacted;
BOOL(WINAPI *rDeleteFile)(LPCTSTR) = DeleteFile;
BOOL(WINAPI *rDeleteFileTransacted)(LPCTSTR, HANDLE) = DeleteFileTransacted;
BOOL(WINAPI *rFindClose)(HANDLE) = FindClose;
HANDLE(WINAPI *rFindFirstFile)(LPCTSTR, LPWIN32_FIND_DATA) = FindFirstFile;
HANDLE(WINAPI *rFindFirstFileEx)(LPCTSTR, FINDEX_INFO_LEVELS, LPVOID, FINDEX_SEARCH_OPS, LPVOID, DWORD) = FindFirstFileEx;
HANDLE(WINAPI *rFindFirstFileNameTransactedW)(LPCWSTR, DWORD, LPDWORD, PWCHAR, HANDLE) = FindFirstFileNameTransactedW;
HANDLE(WINAPI *rFindFirstFileNameW)(LPCWSTR, DWORD, LPDWORD, PWCHAR) = FindFirstFileNameW;
HANDLE(WINAPI *rFindFirstFileTransacted)(LPCTSTR, FINDEX_INFO_LEVELS, LPVOID, FINDEX_SEARCH_OPS, LPVOID, DWORD, HANDLE) = FindFirstFileTransacted;
HANDLE(WINAPI *rFindFirstStreamTransactedW)(LPCWSTR, STREAM_INFO_LEVELS, LPVOID, DWORD, HANDLE) = FindFirstStreamTransactedW;
HANDLE(WINAPI *rFindFirstStreamW)(LPCWSTR, STREAM_INFO_LEVELS, LPVOID, DWORD) = FindFirstStreamW;
BOOL(WINAPI *rFindNextFile)(HANDLE, LPWIN32_FIND_DATA) = FindNextFile;
BOOL(WINAPI *rFindNextFileNameW)(HANDLE, LPDWORD, PWCHAR) = FindNextFileNameW;
BOOL(WINAPI *rFindNextStreamW)(HANDLE, PVOID) = FindNextStreamW;
BOOL(WINAPI *rGetBinaryType)(LPCTSTR, LPDWORD) = GetBinaryType;
DWORD(WINAPI *rGetCompressedFileSize)(LPCTSTR, LPDWORD) = GetCompressedFileSize;
DWORD(WINAPI *rGetCompressedFileSizeTransacted)(LPCTSTR, LPDWORD, HANDLE) = GetCompressedFileSizeTransacted;
DWORD(WINAPI *rGetFileAttributes)(LPCTSTR) = GetFileAttributes;
BOOL(WINAPI *rGetFileAttributesEx)(LPCTSTR, GET_FILEEX_INFO_LEVELS, LPVOID) = GetFileAttributesEx;
BOOL(WINAPI *rGetFileAttributesTransacted)(LPCTSTR, GET_FILEEX_INFO_LEVELS, LPVOID, HANDLE) = GetFileAttributesTransacted;
BOOL(WINAPI *rGetFileBandwidthReservation)(HANDLE, LPDWORD, LPDWORD, LPBOOL, LPDWORD, LPDWORD) = GetFileBandwidthReservation;
BOOL(WINAPI *rGetFileInformationByHandle)(HANDLE, LPBY_HANDLE_FILE_INFORMATION) = GetFileInformationByHandle;
BOOL(WINAPI *rGetFileInformationByHandleEx)(HANDLE, FILE_INFO_BY_HANDLE_CLASS, LPVOID, DWORD) = GetFileInformationByHandleEx;
DWORD(WINAPI *rGetFileSize)(HANDLE, LPDWORD) = GetFileSize;
BOOL(WINAPI *rGetFileSizeEx)(HANDLE, PLARGE_INTEGER) = GetFileSizeEx;
DWORD(WINAPI *rGetFileType)(HANDLE) = GetFileType;
DWORD(WINAPI *rGetFinalPathNameByHandle)(HANDLE, LPTSTR, DWORD, DWORD) = GetFinalPathNameByHandle;
DWORD(WINAPI *rGetFullPathName)(LPCWSTR, DWORD, LPWSTR, LPWSTR*) = GetFullPathName;
DWORD(WINAPI *rGetFullPathNameTransacted)(LPCWSTR, DWORD, LPWSTR, LPWSTR*, HANDLE) = GetFullPathNameTransacted;
DWORD(WINAPI *rGetLongPathName)(LPCTSTR, LPTSTR, DWORD) = GetLongPathName;
DWORD(WINAPI *rGetLongPathNameTransacted)(LPCTSTR, LPTSTR, DWORD, HANDLE) = GetLongPathNameTransacted;
DWORD(WINAPI *rGetShortPathName)(LPCTSTR, LPTSTR, DWORD) = GetShortPathName;
UINT(WINAPI *rGetTempFileName)(LPCTSTR, LPCTSTR, UINT, LPTSTR) = GetTempFileName;
DWORD(WINAPI *rGetTempPath)(DWORD, LPTSTR) = GetTempPath;
BOOL(WINAPI *rMoveFile)(LPCTSTR, LPCTSTR) = MoveFile;
BOOL(WINAPI *rMoveFileEx)(LPCTSTR, LPCTSTR, DWORD) = MoveFileEx;
BOOL(WINAPI *rMoveFileTransacted)(LPCTSTR, LPCTSTR, LPPROGRESS_ROUTINE, LPVOID, DWORD, HANDLE) = MoveFileTransacted;
BOOL(WINAPI *rMoveFileWithProgress)(LPCTSTR, LPCTSTR, LPPROGRESS_ROUTINE, LPVOID, DWORD) = MoveFileWithProgress;
HFILE(WINAPI *rOpenFile)(LPCSTR, LPOFSTRUCT, UINT) = OpenFile;
HANDLE(WINAPI *rOpenFileById)(HANDLE, LPFILE_ID_DESCRIPTOR, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD) = OpenFileById;
HANDLE(WINAPI *rReOpenFile)(HANDLE, DWORD, DWORD, DWORD) = ReOpenFile;
BOOL(WINAPI *rReplaceFile)(LPCTSTR, LPCTSTR, LPCTSTR, DWORD, LPVOID, LPVOID) = ReplaceFile;
DWORD(WINAPI *rSearchPath)(LPCTSTR, LPCTSTR, LPCTSTR, DWORD, LPWSTR, LPWSTR*) = SearchPath;
void(WINAPI *rSetFileApisToANSI)(void) = SetFileApisToANSI;
void(WINAPI *rSetFileApisToOEM)(void) = SetFileApisToOEM;
BOOL(WINAPI *rSetFileAttributes)(LPCWSTR, DWORD) = SetFileAttributes;
BOOL(WINAPI *rSetFileAttributesTransacted)(LPCWSTR, DWORD, HANDLE) = SetFileAttributesTransacted;
BOOL(WINAPI *rSetFileBandwidthReservation)(HANDLE, DWORD, DWORD, BOOL, LPDWORD, LPDWORD) = SetFileBandwidthReservation;
BOOL(WINAPI *rSetFileInformationByHandle)(HANDLE, FILE_INFO_BY_HANDLE_CLASS, LPVOID, DWORD) = SetFileInformationByHandle;
BOOL(WINAPI *rSetFileShortName)(HANDLE, LPCTSTR) = SetFileShortName;
BOOL(WINAPI *rSetFileValidData)(HANDLE, LONGLONG) = SetFileValidData;
BOOL(WINAPI *rSetSearchPathMode)(DWORD) = SetSearchPathMode;
//------------File Management functions end------------//

//------------Registry functions start------------//
LONG (WINAPI *rRegCloseKey)(HKEY) = RegCloseKey;
LONG (WINAPI *rRegCreateKeyEx)(HKEY, LPCTSTR, DWORD, LPTSTR, DWORD, REGSAM, LPSECURITY_ATTRIBUTES, PHKEY, LPDWORD) = RegCreateKeyEx;
LONG (WINAPI *rRegConnectRegistry)(LPCTSTR, HKEY, PHKEY) = RegConnectRegistry;
LONG (WINAPI *rRegDeleteKey)(HKEY, LPCTSTR) = RegDeleteKey;
LONG (WINAPI *rRegDeleteKeyEx)(HKEY, LPCTSTR, REGSAM, DWORD) = RegDeleteKeyEx;
LONG (WINAPI *rRegDeleteValue)(HKEY, LPCTSTR) = RegDeleteValue;
LONG (WINAPI *rRegGetValue)(HKEY, LPCTSTR, LPCTSTR, DWORD, LPDWORD, PVOID, LPDWORD) = RegGetValue;
LONG (WINAPI *rRegLoadAppKey)(LPCTSTR, PHKEY, REGSAM, DWORD, DWORD) = RegLoadAppKey;
LONG (WINAPI *rRegLoadKey)(HKEY, LPCTSTR, LPCTSTR) = RegLoadKey;
LONG (WINAPI *rRegOpenKeyEx)(HKEY, LPCTSTR, DWORD, REGSAM, PHKEY) = RegOpenKeyEx;
LONG (WINAPI *rRegReplaceKey)(HKEY, LPCTSTR, LPCTSTR, LPCTSTR) = RegReplaceKey;
LONG (WINAPI *rRegRestoreKey)(HKEY, LPCTSTR, DWORD) = RegRestoreKey; 
LONG (WINAPI *rRegSaveKey)(HKEY, LPCTSTR, LPSECURITY_ATTRIBUTES) = RegSaveKey;
LONG (WINAPI *rRegSaveKeyEx)(HKEY, LPCTSTR, LPSECURITY_ATTRIBUTES, DWORD) = RegSaveKeyEx;
LONG (WINAPI *rRegSetKeyValue)(HKEY, LPCTSTR, LPCTSTR, DWORD, LPCVOID, DWORD) = RegSetKeyValue;
LONG (WINAPI *rRegSetValueEx)(HKEY, LPCTSTR, DWORD, DWORD, const BYTE*, DWORD) = RegSetValueEx;
LONG (WINAPI *rRegUnLoadKey)(HKEY, LPCTSTR) = RegUnLoadKey;
//------------Registry functions end------------//

//------------File Management functions start------------//
BOOL WINAPI cCancelIo(HANDLE handleval1)
{
	WriteToLogFile("CancelIo"); return rCancelIo(handleval1);
}
BOOL WINAPI cCancelIoEx(HANDLE handleval1, LPOVERLAPPED  lpoverlappedval2)
{
	WriteToLogFile("CancelIoEx"); return rCancelIoEx(handleval1, lpoverlappedval2);
}
BOOL WINAPI cCancelSynchronousIo(HANDLE handleval1)
{
	WriteToLogFile("CancelSynchronousIo"); return rCancelSynchronousIo(handleval1);
}
HANDLE WINAPI cCreateIoCompletionPort(HANDLE handleval1, HANDLE  handleval2, ULONG_PTR  ulong_ptrval3, DWORD  dwordval4)
{
	WriteToLogFile("CreateIoCompletionPort"); return rCreateIoCompletionPort(handleval1, handleval2, ulong_ptrval3, dwordval4);
}
BOOL WINAPI cFlushFileBuffers(HANDLE handleval1)
{
	WriteToLogFile("FlushFileBuffers"); return rFlushFileBuffers(handleval1);
}
BOOL WINAPI cGetQueuedCompletionStatus(HANDLE handleval1, LPDWORD  lpdwordval2, PULONG_PTR  pulong_ptrval3, LPOVERLAPPED*  lpoverlappedval4, DWORD  dwordval5)
{
	WriteToLogFile("GetQueuedCompletionStatus"); return rGetQueuedCompletionStatus(handleval1, lpdwordval2, pulong_ptrval3, lpoverlappedval4, dwordval5);
}
BOOL WINAPI cGetQueuedCompletionStatusEx(HANDLE handleval1, LPOVERLAPPED_ENTRY  lpoverlapped_entryval2, ULONG  ulongval3, PULONG  pulongval4, DWORD  dwordval5, BOOL  boolval6)
{
	WriteToLogFile("GetQueuedCompletionStatusEx"); return rGetQueuedCompletionStatusEx(handleval1, lpoverlapped_entryval2, ulongval3, pulongval4, dwordval5, boolval6);
}
BOOL WINAPI cLockFile(HANDLE handleval1, DWORD  dwordval2, DWORD  dwordval3, DWORD  dwordval4, DWORD  dwordval5)
{
	WriteToLogFile("LockFile"); return rLockFile(handleval1, dwordval2, dwordval3, dwordval4, dwordval5);
}
BOOL WINAPI cLockFileEx(HANDLE handleval1, DWORD  dwordval2, DWORD  dwordval3, DWORD  dwordval4, DWORD  dwordval5, LPOVERLAPPED  lpoverlappedval6)
{
	WriteToLogFile("LockFileEx"); return rLockFileEx(handleval1, dwordval2, dwordval3, dwordval4, dwordval5, lpoverlappedval6);
}
BOOL WINAPI cPostQueuedCompletionStatus(HANDLE handleval1, DWORD  dwordval2, ULONG_PTR  ulong_ptrval3, LPOVERLAPPED  lpoverlappedval4)
{
	WriteToLogFile("PostQueuedCompletionStatus"); return rPostQueuedCompletionStatus(handleval1, dwordval2, ulong_ptrval3, lpoverlappedval4);
}
BOOL WINAPI cReadFile(HANDLE handleval1, LPVOID  lpvoidval2, DWORD  dwordval3, LPDWORD  lpdwordval4, LPOVERLAPPED  lpoverlappedval5)
{
	WriteToLogFile("ReadFile"); return rReadFile(handleval1, lpvoidval2, dwordval3, lpdwordval4, lpoverlappedval5);
}
BOOL WINAPI cReadFileEx(HANDLE handleval1, LPVOID  lpvoidval2, DWORD  dwordval3, LPOVERLAPPED  lpoverlappedval4, LPOVERLAPPED_COMPLETION_ROUTINE  lpoverlapped_completion_routineval5)
{
	WriteToLogFile("ReadFileEx"); return rReadFileEx(handleval1, lpvoidval2, dwordval3, lpoverlappedval4, lpoverlapped_completion_routineval5);
}
BOOL WINAPI cReadFileScatter(HANDLE handleval1, FILE_SEGMENT_ELEMENT*  file_segment_elementval2, DWORD  dwordval3, LPDWORD  lpdwordval4, LPOVERLAPPED  lpoverlappedval5)
{
	WriteToLogFile("ReadFileScatter"); return rReadFileScatter(handleval1, file_segment_elementval2, dwordval3, lpdwordval4, lpoverlappedval5);
}
BOOL WINAPI cSetEndOfFile(HANDLE handleval1)
{
	WriteToLogFile("SetEndOfFile"); return rSetEndOfFile(handleval1);
}
BOOL WINAPI cSetFileCompletionNotificationModes(HANDLE handleval1, UCHAR  ucharval2)
{
	WriteToLogFile("SetFileCompletionNotificationModes"); return rSetFileCompletionNotificationModes(handleval1, ucharval2);
}
BOOL WINAPI cSetFileIoOverlappedRange(HANDLE handleval1, PUCHAR  pucharval2, ULONG  ulongval3)
{
	WriteToLogFile("SetFileIoOverlappedRange"); return rSetFileIoOverlappedRange(handleval1, pucharval2, ulongval3);
}
DWORD WINAPI cSetFilePointer(HANDLE handleval1, LONG  longval2, PLONG  plongval3, DWORD  dwordval4)
{
	WriteToLogFile("SetFilePointer"); return rSetFilePointer(handleval1, longval2, plongval3, dwordval4);
}
BOOL WINAPI cSetFilePointerEx(HANDLE handleval1, LARGE_INTEGER  large_integerval2, PLARGE_INTEGER  plarge_integerval3, DWORD  dwordval4)
{
	WriteToLogFile("SetFilePointerEx"); return rSetFilePointerEx(handleval1, large_integerval2, plarge_integerval3, dwordval4);
}
BOOL WINAPI cUnlockFile(HANDLE handleval1, DWORD  dwordval2, DWORD  dwordval3, DWORD  dwordval4, DWORD  dwordval5)
{
	WriteToLogFile("UnlockFile"); return rUnlockFile(handleval1, dwordval2, dwordval3, dwordval4, dwordval5);
}
BOOL WINAPI cUnlockFileEx(HANDLE handleval1, DWORD  dwordval2, DWORD  dwordval3, DWORD  dwordval4, LPOVERLAPPED  lpoverlappedval5)
{
	WriteToLogFile("UnlockFileEx"); return rUnlockFileEx(handleval1, dwordval2, dwordval3, dwordval4, lpoverlappedval5);
}
BOOL WINAPI cWriteFile(HANDLE handleval1, LPCVOID  lpcvoidval2, DWORD  dwordval3, LPDWORD  lpdwordval4, LPOVERLAPPED  lpoverlappedval5)
{
	WriteToLogFile("WriteFile"); return rWriteFile(handleval1, lpcvoidval2, dwordval3, lpdwordval4, lpoverlappedval5);
}
BOOL WINAPI cWriteFileEx(HANDLE handleval1, LPCVOID  lpcvoidval2, DWORD  dwordval3, LPOVERLAPPED  lpoverlappedval4, LPOVERLAPPED_COMPLETION_ROUTINE  lpoverlapped_completion_routineval5)
{
	WriteToLogFile("WriteFileEx"); return rWriteFileEx(handleval1, lpcvoidval2, dwordval3, lpoverlappedval4, lpoverlapped_completion_routineval5);
}
BOOL WINAPI cWriteFileGather(HANDLE handleval1, FILE_SEGMENT_ELEMENT*  file_segment_elementval2, DWORD  dwordval3, LPDWORD  lpdwordval4, LPOVERLAPPED  lpoverlappedval5)
{
	WriteToLogFile("WriteFileGather"); return rWriteFileGather(handleval1, file_segment_elementval2, dwordval3, lpdwordval4, lpoverlappedval5);
}
DWORD WINAPI cAddUsersToEncryptedFile(LPCWSTR lpcwstrval1, PENCRYPTION_CERTIFICATE_LIST  pencryption_certificate_listval2)
{
	WriteToLogFile("AddUsersToEncryptedFile"); return rAddUsersToEncryptedFile(lpcwstrval1, pencryption_certificate_listval2);
}
void WINAPI cCloseEncryptedFileRaw(PVOID pvoidval1)
{
	WriteToLogFile("CloseEncryptedFileRaw"); return rCloseEncryptedFileRaw(pvoidval1);
}
BOOL WINAPI cDecryptFile(LPCTSTR lpctstrval1, DWORD  dwordval2)
{
	WriteToLogFile("DecryptFile"); return rDecryptFile(lpctstrval1, dwordval2);
}
DWORD WINAPI cDuplicateEncryptionInfoFile(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, DWORD  dwordval3, DWORD  dwordval4, LPSECURITY_ATTRIBUTES  lpsecurity_attributesval5)
{
	WriteToLogFile("DuplicateEncryptionInfoFile"); return rDuplicateEncryptionInfoFile(lpctstrval1, lpctstrval2, dwordval3, dwordval4, lpsecurity_attributesval5);
}
BOOL WINAPI cEncryptFile(LPCTSTR lpctstrval1)
{
	WriteToLogFile("EncryptFile"); return rEncryptFile(lpctstrval1);
}
BOOL WINAPI cEncryptionDisable(LPCWSTR lpcwstrval1, BOOL  boolval2)
{
	WriteToLogFile("EncryptionDisable"); return rEncryptionDisable(lpcwstrval1, boolval2);
}
BOOL WINAPI cFileEncryptionStatus(LPCTSTR lpctstrval1, LPDWORD  lpdwordval2)
{
	WriteToLogFile("FileEncryptionStatus"); return rFileEncryptionStatus(lpctstrval1, lpdwordval2);
}
void WINAPI cFreeEncryptionCertificateHashList(PENCRYPTION_CERTIFICATE_HASH_LIST pencryption_certificate_hash_listval1)
{
	WriteToLogFile("FreeEncryptionCertificateHashList"); return rFreeEncryptionCertificateHashList(pencryption_certificate_hash_listval1);
}
DWORD WINAPI cOpenEncryptedFileRaw(LPCTSTR lpctstrval1, ULONG  ulongval2, PVOID*  pvoidval3)
{
	WriteToLogFile("OpenEncryptedFileRaw"); return rOpenEncryptedFileRaw(lpctstrval1, ulongval2, pvoidval3);
}
DWORD WINAPI cQueryRecoveryAgentsOnEncryptedFile(LPCWSTR lpcwstrval1, PENCRYPTION_CERTIFICATE_HASH_LIST*  pencryption_certificate_hash_listval2)
{
	WriteToLogFile("QueryRecoveryAgentsOnEncryptedFile"); return rQueryRecoveryAgentsOnEncryptedFile(lpcwstrval1, pencryption_certificate_hash_listval2);
}
DWORD WINAPI cQueryUsersOnEncryptedFile(LPCWSTR lpcwstrval1, PENCRYPTION_CERTIFICATE_HASH_LIST*  pencryption_certificate_hash_listval2)
{
	WriteToLogFile("QueryUsersOnEncryptedFile"); return rQueryUsersOnEncryptedFile(lpcwstrval1, pencryption_certificate_hash_listval2);
}
DWORD WINAPI cReadEncryptedFileRaw(PFE_EXPORT_FUNC pfe_export_funcval1, PVOID  pvoidval2, PVOID  pvoidval3)
{
	WriteToLogFile("ReadEncryptedFileRaw"); return rReadEncryptedFileRaw(pfe_export_funcval1, pvoidval2, pvoidval3);
}
DWORD WINAPI cRemoveUsersFromEncryptedFile(LPCWSTR lpcwstrval1, PENCRYPTION_CERTIFICATE_HASH_LIST  pencryption_certificate_hash_listval2)
{
	WriteToLogFile("RemoveUsersFromEncryptedFile"); return rRemoveUsersFromEncryptedFile(lpcwstrval1, pencryption_certificate_hash_listval2);
}
DWORD WINAPI cWriteEncryptedFileRaw(PFE_IMPORT_FUNC pfe_import_funcval1, PVOID  pvoidval2, PVOID  pvoidval3)
{
	WriteToLogFile("WriteEncryptedFileRaw"); return rWriteEncryptedFileRaw(pfe_import_funcval1, pvoidval2, pvoidval3);
}
DWORD WINAPI cSetUserFileEncryptionKey(PENCRYPTION_CERTIFICATE pencryption_certificateval1)
{
	WriteToLogFile("SetUserFileEncryptionKey"); return rSetUserFileEncryptionKey(pencryption_certificateval1);
}
BOOL WINAPI cAreFileApisANSI()
{
	WriteToLogFile("AreFileApisANSI"); return rAreFileApisANSI();
}
BOOL WINAPI cCheckNameLegalDOS8Dot3(LPCTSTR lpctstrval1, LPSTR  lpstrval2, DWORD  dwordval3, PBOOL  pboolval4, PBOOL  pboolval5)
{
	WriteToLogFile("CheckNameLegalDOS8Dot3"); return rCheckNameLegalDOS8Dot3(lpctstrval1, lpstrval2, dwordval3, pboolval4, pboolval5);
}
BOOL WINAPI cCopyFile(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, BOOL  boolval3)
{
	WriteToLogFile("CopyFile"); return rCopyFile(lpctstrval1, lpctstrval2, boolval3);
}
BOOL WINAPI cCopyFileEx(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPPROGRESS_ROUTINE  lpprogress_routineval3, LPVOID  lpvoidval4, LPBOOL  lpboolval5, DWORD  dwordval6)
{
	WriteToLogFile("CopyFileEx"); return rCopyFileEx(lpctstrval1, lpctstrval2, lpprogress_routineval3, lpvoidval4, lpboolval5, dwordval6);
}
BOOL WINAPI cCopyFileTransacted(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPPROGRESS_ROUTINE  lpprogress_routineval3, LPVOID  lpvoidval4, LPBOOL  lpboolval5, DWORD  dwordval6, HANDLE  handleval7)
{
	WriteToLogFile("CopyFileTransacted"); return rCopyFileTransacted(lpctstrval1, lpctstrval2, lpprogress_routineval3, lpvoidval4, lpboolval5, dwordval6, handleval7);
}
HANDLE WINAPI cCreateFile(LPCTSTR lpctstrval1, DWORD  dwordval2, DWORD  dwordval3, LPSECURITY_ATTRIBUTES  lpsecurity_attributesval4, DWORD  dwordval5, DWORD  dwordval6, HANDLE  handleval7)
{
	WriteToLogFile("CreateFile"); return rCreateFile(lpctstrval1, dwordval2, dwordval3, lpsecurity_attributesval4, dwordval5, dwordval6, handleval7);
}
HANDLE WINAPI cCreateFileTransacted(LPCTSTR lpctstrval1, DWORD  dwordval2, DWORD  dwordval3, LPSECURITY_ATTRIBUTES  lpsecurity_attributesval4, DWORD  dwordval5, DWORD  dwordval6, HANDLE  handleval7, HANDLE  handleval8, PUSHORT  pushortval9, PVOID  pvoidval10)
{
	WriteToLogFile("CreateFileTransacted"); return rCreateFileTransacted(lpctstrval1, dwordval2, dwordval3, lpsecurity_attributesval4, dwordval5, dwordval6, handleval7, handleval8, pushortval9, pvoidval10);
}
BOOL WINAPI cCreateHardLink(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPSECURITY_ATTRIBUTES  lpsecurity_attributesval3)
{
	WriteToLogFile("CreateHardLink"); return rCreateHardLink(lpctstrval1, lpctstrval2, lpsecurity_attributesval3);
}
BOOL WINAPI cCreateHardLinkTransacted(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPSECURITY_ATTRIBUTES  lpsecurity_attributesval3, HANDLE  handleval4)
{
	WriteToLogFile("CreateHardLinkTransacted"); return rCreateHardLinkTransacted(lpctstrval1, lpctstrval2, lpsecurity_attributesval3, handleval4);
}
BOOLEAN WINAPI cCreateSymbolicLink(LPCWSTR lpcwstrval1, LPCWSTR  lpcwstrval2, DWORD  dwordval3)
{
	WriteToLogFile("CreateSymbolicLink"); return rCreateSymbolicLink(lpcwstrval1, lpcwstrval2, dwordval3);
}
BOOLEAN WINAPI cCreateSymbolicLinkTransacted(LPCWSTR lpcwstrval1, LPCWSTR  lpcwstrval2, DWORD  dwordval3, HANDLE  handleval4)
{
	WriteToLogFile("CreateSymbolicLinkTransacted"); return rCreateSymbolicLinkTransacted(lpcwstrval1, lpcwstrval2, dwordval3, handleval4);
}
BOOL WINAPI cDeleteFile(LPCTSTR lpctstrval1)
{
	WriteToLogFile("DeleteFile"); return rDeleteFile(lpctstrval1);
}
BOOL WINAPI cDeleteFileTransacted(LPCTSTR lpctstrval1, HANDLE  handleval2)
{
	WriteToLogFile("DeleteFileTransacted"); return rDeleteFileTransacted(lpctstrval1, handleval2);
}
BOOL WINAPI cFindClose(HANDLE handleval1)
{
	WriteToLogFile("FindClose"); return rFindClose(handleval1);
}
HANDLE WINAPI cFindFirstFile(LPCTSTR lpctstrval1, LPWIN32_FIND_DATA  lpwin32_find_dataval2)
{
	WriteToLogFile("FindFirstFile"); return rFindFirstFile(lpctstrval1, lpwin32_find_dataval2);
}
HANDLE WINAPI cFindFirstFileEx(LPCTSTR lpctstrval1, FINDEX_INFO_LEVELS  findex_info_levelsval2, LPVOID  lpvoidval3, FINDEX_SEARCH_OPS  findex_search_opsval4, LPVOID  lpvoidval5, DWORD  dwordval6)
{
	WriteToLogFile("FindFirstFileEx"); return rFindFirstFileEx(lpctstrval1, findex_info_levelsval2, lpvoidval3, findex_search_opsval4, lpvoidval5, dwordval6);
}
HANDLE WINAPI cFindFirstFileNameTransactedW(LPCWSTR lpcwstrval1, DWORD  dwordval2, LPDWORD  lpdwordval3, PWCHAR  pwcharval4, HANDLE  handleval5)
{
	WriteToLogFile("FindFirstFileNameTransactedW"); return rFindFirstFileNameTransactedW(lpcwstrval1, dwordval2, lpdwordval3, pwcharval4, handleval5);
}
HANDLE WINAPI cFindFirstFileNameW(LPCWSTR lpcwstrval1, DWORD  dwordval2, LPDWORD  lpdwordval3, PWCHAR  pwcharval4)
{
	WriteToLogFile("FindFirstFileNameW"); return rFindFirstFileNameW(lpcwstrval1, dwordval2, lpdwordval3, pwcharval4);
}
HANDLE WINAPI cFindFirstFileTransacted(LPCTSTR lpctstrval1, FINDEX_INFO_LEVELS  findex_info_levelsval2, LPVOID  lpvoidval3, FINDEX_SEARCH_OPS  findex_search_opsval4, LPVOID  lpvoidval5, DWORD  dwordval6, HANDLE  handleval7)
{
	WriteToLogFile("FindFirstFileTransacted"); return rFindFirstFileTransacted(lpctstrval1, findex_info_levelsval2, lpvoidval3, findex_search_opsval4, lpvoidval5, dwordval6, handleval7);
}
HANDLE WINAPI cFindFirstStreamTransactedW(LPCWSTR lpcwstrval1, STREAM_INFO_LEVELS  stream_info_levelsval2, LPVOID  lpvoidval3, DWORD  dwordval4, HANDLE  handleval5)
{
	WriteToLogFile("FindFirstStreamTransactedW"); return rFindFirstStreamTransactedW(lpcwstrval1, stream_info_levelsval2, lpvoidval3, dwordval4, handleval5);
}
HANDLE WINAPI cFindFirstStreamW(LPCWSTR lpcwstrval1, STREAM_INFO_LEVELS  stream_info_levelsval2, LPVOID  lpvoidval3, DWORD  dwordval4)
{
	WriteToLogFile("FindFirstStreamW"); return rFindFirstStreamW(lpcwstrval1, stream_info_levelsval2, lpvoidval3, dwordval4);
}
BOOL WINAPI cFindNextFile(HANDLE handleval1, LPWIN32_FIND_DATA  lpwin32_find_dataval2)
{
	WriteToLogFile("FindNextFile"); return rFindNextFile(handleval1, lpwin32_find_dataval2);
}
BOOL WINAPI cFindNextFileNameW(HANDLE handleval1, LPDWORD  lpdwordval2, PWCHAR  pwcharval3)
{
	WriteToLogFile("FindNextFileNameW"); return rFindNextFileNameW(handleval1, lpdwordval2, pwcharval3);
}
BOOL WINAPI cFindNextStreamW(HANDLE handleval1, PVOID  pvoidval2)
{
	WriteToLogFile("FindNextStreamW"); return rFindNextStreamW(handleval1, pvoidval2);
}
BOOL WINAPI cGetBinaryType(LPCTSTR lpctstrval1, LPDWORD  lpdwordval2)
{
	WriteToLogFile("GetBinaryType"); return rGetBinaryType(lpctstrval1, lpdwordval2);
}
DWORD WINAPI cGetCompressedFileSize(LPCTSTR lpctstrval1, LPDWORD  lpdwordval2)
{
	WriteToLogFile("GetCompressedFileSize"); return rGetCompressedFileSize(lpctstrval1, lpdwordval2);
}
DWORD WINAPI cGetCompressedFileSizeTransacted(LPCTSTR lpctstrval1, LPDWORD  lpdwordval2, HANDLE  handleval3)
{
	WriteToLogFile("GetCompressedFileSizeTransacted"); return rGetCompressedFileSizeTransacted(lpctstrval1, lpdwordval2, handleval3);
}
DWORD WINAPI cGetFileAttributes(LPCTSTR lpctstrval1)
{
	WriteToLogFile("GetFileAttributes"); return rGetFileAttributes(lpctstrval1);
}
BOOL WINAPI cGetFileAttributesEx(LPCTSTR lpctstrval1, GET_FILEEX_INFO_LEVELS  get_fileex_info_levelsval2, LPVOID  lpvoidval3)
{
	WriteToLogFile("GetFileAttributesEx"); return rGetFileAttributesEx(lpctstrval1, get_fileex_info_levelsval2, lpvoidval3);
}
BOOL WINAPI cGetFileAttributesTransacted(LPCTSTR lpctstrval1, GET_FILEEX_INFO_LEVELS  get_fileex_info_levelsval2, LPVOID  lpvoidval3, HANDLE  handleval4)
{
	WriteToLogFile("GetFileAttributesTransacted"); return rGetFileAttributesTransacted(lpctstrval1, get_fileex_info_levelsval2, lpvoidval3, handleval4);
}
BOOL WINAPI cGetFileBandwidthReservation(HANDLE handleval1, LPDWORD  lpdwordval2, LPDWORD  lpdwordval3, LPBOOL  lpboolval4, LPDWORD  lpdwordval5, LPDWORD  lpdwordval6)
{
	WriteToLogFile("GetFileBandwidthReservation"); return rGetFileBandwidthReservation(handleval1, lpdwordval2, lpdwordval3, lpboolval4, lpdwordval5, lpdwordval6);
}
BOOL WINAPI cGetFileInformationByHandle(HANDLE handleval1, LPBY_HANDLE_FILE_INFORMATION  lpby_handle_file_informationval2)
{
	WriteToLogFile("GetFileInformationByHandle"); return rGetFileInformationByHandle(handleval1, lpby_handle_file_informationval2);
}
BOOL WINAPI cGetFileInformationByHandleEx(HANDLE handleval1, FILE_INFO_BY_HANDLE_CLASS  file_info_by_handle_classval2, LPVOID  lpvoidval3, DWORD  dwordval4)
{
	WriteToLogFile("GetFileInformationByHandleEx"); return rGetFileInformationByHandleEx(handleval1, file_info_by_handle_classval2, lpvoidval3, dwordval4);
}
DWORD WINAPI cGetFileSize(HANDLE handleval1, LPDWORD  lpdwordval2)
{
	WriteToLogFile("GetFileSize"); return rGetFileSize(handleval1, lpdwordval2);
}
BOOL WINAPI cGetFileSizeEx(HANDLE handleval1, PLARGE_INTEGER  plarge_integerval2)
{
	WriteToLogFile("GetFileSizeEx"); return rGetFileSizeEx(handleval1, plarge_integerval2);
}
DWORD WINAPI cGetFileType(HANDLE handleval1)
{
	WriteToLogFile("GetFileType"); return rGetFileType(handleval1);
}
DWORD WINAPI cGetFinalPathNameByHandle(HANDLE handleval1, LPTSTR  lptstrval2, DWORD  dwordval3, DWORD  dwordval4)
{
	WriteToLogFile("GetFinalPathNameByHandle"); return rGetFinalPathNameByHandle(handleval1, lptstrval2, dwordval3, dwordval4);
}
DWORD WINAPI cGetFullPathName(LPCWSTR lpcwstrval1, DWORD  dwordval2, LPWSTR  lpwstrval3, LPWSTR*  lpwstrval4)
{
	WriteToLogFile("GetFullPathName"); return rGetFullPathName(lpcwstrval1, dwordval2, lpwstrval3, lpwstrval4);
}
DWORD WINAPI cGetFullPathNameTransacted(LPCWSTR lpcwstrval1, DWORD  dwordval2, LPWSTR  lpwstrval3, LPWSTR*  lpwstrval4, HANDLE  handleval5)
{
	WriteToLogFile("GetFullPathNameTransacted"); return rGetFullPathNameTransacted(lpcwstrval1, dwordval2, lpwstrval3, lpwstrval4, handleval5);
}
DWORD WINAPI cGetLongPathName(LPCTSTR lpctstrval1, LPTSTR  lptstrval2, DWORD  dwordval3)
{
	WriteToLogFile("GetLongPathName"); return rGetLongPathName(lpctstrval1, lptstrval2, dwordval3);
}
DWORD WINAPI cGetLongPathNameTransacted(LPCTSTR lpctstrval1, LPTSTR  lptstrval2, DWORD  dwordval3, HANDLE  handleval4)
{
	WriteToLogFile("GetLongPathNameTransacted"); return rGetLongPathNameTransacted(lpctstrval1, lptstrval2, dwordval3, handleval4);
}
DWORD WINAPI cGetShortPathName(LPCTSTR lpctstrval1, LPTSTR  lptstrval2, DWORD  dwordval3)
{
	WriteToLogFile("GetShortPathName"); return rGetShortPathName(lpctstrval1, lptstrval2, dwordval3);
}
UINT WINAPI cGetTempFileName(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, UINT  uintval3, LPTSTR  lptstrval4)
{
	WriteToLogFile("GetTempFileName"); return rGetTempFileName(lpctstrval1, lpctstrval2, uintval3, lptstrval4);
}
DWORD WINAPI cGetTempPath(DWORD dwordval1, LPTSTR  lptstrval2)
{
	WriteToLogFile("GetTempPath"); return rGetTempPath(dwordval1, lptstrval2);
}
BOOL WINAPI cMoveFile(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2)
{
	WriteToLogFile("MoveFile"); return rMoveFile(lpctstrval1, lpctstrval2);
}
BOOL WINAPI cMoveFileEx(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, DWORD  dwordval3)
{
	WriteToLogFile("MoveFileEx"); return rMoveFileEx(lpctstrval1, lpctstrval2, dwordval3);
}
BOOL WINAPI cMoveFileTransacted(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPPROGRESS_ROUTINE  lpprogress_routineval3, LPVOID  lpvoidval4, DWORD  dwordval5, HANDLE  handleval6)
{
	WriteToLogFile("MoveFileTransacted"); return rMoveFileTransacted(lpctstrval1, lpctstrval2, lpprogress_routineval3, lpvoidval4, dwordval5, handleval6);
}
BOOL WINAPI cMoveFileWithProgress(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPPROGRESS_ROUTINE  lpprogress_routineval3, LPVOID  lpvoidval4, DWORD  dwordval5)
{
	WriteToLogFile("MoveFileWithProgress"); return rMoveFileWithProgress(lpctstrval1, lpctstrval2, lpprogress_routineval3, lpvoidval4, dwordval5);
}
HFILE WINAPI cOpenFile(LPCSTR lpcstrval1, LPOFSTRUCT  lpofstructval2, UINT  uintval3)
{
	WriteToLogFile("OpenFile"); return rOpenFile(lpcstrval1, lpofstructval2, uintval3);
}
HANDLE WINAPI cOpenFileById(HANDLE handleval1, LPFILE_ID_DESCRIPTOR  lpfile_id_descriptorval2, DWORD  dwordval3, DWORD  dwordval4, LPSECURITY_ATTRIBUTES  lpsecurity_attributesval5, DWORD  dwordval6)
{
	WriteToLogFile("OpenFileById"); return rOpenFileById(handleval1, lpfile_id_descriptorval2, dwordval3, dwordval4, lpsecurity_attributesval5, dwordval6);
}
HANDLE WINAPI cReOpenFile(HANDLE handleval1, DWORD  dwordval2, DWORD  dwordval3, DWORD  dwordval4)
{
	WriteToLogFile("ReOpenFile"); return rReOpenFile(handleval1, dwordval2, dwordval3, dwordval4);
}
BOOL WINAPI cReplaceFile(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPCTSTR  lpctstrval3, DWORD  dwordval4, LPVOID  lpvoidval5, LPVOID  lpvoidval6)
{
	WriteToLogFile("ReplaceFile"); return rReplaceFile(lpctstrval1, lpctstrval2, lpctstrval3, dwordval4, lpvoidval5, lpvoidval6);
}
DWORD WINAPI cSearchPath(LPCTSTR lpctstrval1, LPCTSTR  lpctstrval2, LPCTSTR  lpctstrval3, DWORD  dwordval4, LPWSTR  lpwstrval5, LPWSTR*  lpwstrval6)
{
	WriteToLogFile("SearchPath"); return rSearchPath(lpctstrval1, lpctstrval2, lpctstrval3, dwordval4, lpwstrval5, lpwstrval6);
}
void WINAPI cSetFileApisToANSI()
{
	WriteToLogFile("SetFileApisToANSI"); return rSetFileApisToANSI();
}
void WINAPI cSetFileApisToOEM()
{
	WriteToLogFile("SetFileApisToOEM"); return rSetFileApisToOEM();
}
BOOL WINAPI cSetFileAttributes(LPCWSTR lpcwstrval1, DWORD  dwordval2)
{
	WriteToLogFile("SetFileAttributes"); return rSetFileAttributes(lpcwstrval1, dwordval2);
}
BOOL WINAPI cSetFileAttributesTransacted(LPCWSTR lpcwstrval1, DWORD  dwordval2, HANDLE  handleval3)
{
	WriteToLogFile("SetFileAttributesTransacted"); return rSetFileAttributesTransacted(lpcwstrval1, dwordval2, handleval3);
}
BOOL WINAPI cSetFileBandwidthReservation(HANDLE handleval1, DWORD  dwordval2, DWORD  dwordval3, BOOL  boolval4, LPDWORD  lpdwordval5, LPDWORD  lpdwordval6)
{
	WriteToLogFile("SetFileBandwidthReservation"); return rSetFileBandwidthReservation(handleval1, dwordval2, dwordval3, boolval4, lpdwordval5, lpdwordval6);
}
BOOL WINAPI cSetFileInformationByHandle(HANDLE handleval1, FILE_INFO_BY_HANDLE_CLASS  file_info_by_handle_classval2, LPVOID  lpvoidval3, DWORD  dwordval4)
{
	WriteToLogFile("SetFileInformationByHandle"); return rSetFileInformationByHandle(handleval1, file_info_by_handle_classval2, lpvoidval3, dwordval4);
}
BOOL WINAPI cSetFileShortName(HANDLE handleval1, LPCTSTR  lpctstrval2)
{
	WriteToLogFile("SetFileShortName"); return rSetFileShortName(handleval1, lpctstrval2);
}
BOOL WINAPI cSetFileValidData(HANDLE handleval1, LONGLONG  longlongval2)
{
	WriteToLogFile("SetFileValidData"); return rSetFileValidData(handleval1, longlongval2);
}
BOOL WINAPI cSetSearchPathMode(DWORD dwordval1)
{
	WriteToLogFile("SetSearchPathMode"); return rSetSearchPathMode(dwordval1);
}
//------------File Management functions end------------//

//------------Registry functions start-----------------//
LONG WINAPI cRegCloseKey(HKEY hKey)
{
	WriteToLogFile("RegCloseKey"); return rRegCloseKey(hKey);
}

LONG WINAPI cRegCreateKeyEx(HKEY hKey, LPCTSTR lpSubKey, DWORD Reserved,
							LPTSTR lpClass, DWORD dwOptions, REGSAM samDesired,
							LPSECURITY_ATTRIBUTES lpSecurityAttributes,
							PHKEY phkResult, LPDWORD lpdwDisposition)
{
	WriteToLogFile("RegCreateKeyEx"); return rRegCreateKeyEx(hKey, lpSubKey, Reserved, lpClass, dwOptions, samDesired, lpSecurityAttributes, phkResult, lpdwDisposition);
}

LONG WINAPI cRegConnectRegistry(LPCTSTR lpMachineName, HKEY hKey, PHKEY phkResult)
{
	WriteToLogFile("RegConnectRegistry"); return rRegConnectRegistry(lpMachineName, hKey, phkResult);
}

LONG WINAPI cRegDeleteKey(HKEY hKey, LPCTSTR lpSubKey)
{
	WriteToLogFile("RegDeleteKey"); return rRegDeleteKey(hKey, lpSubKey);
}

LONG WINAPI cRegDeleteKeyEx(HKEY hKey, LPCTSTR lpSubKey, REGSAM samDesired, DWORD Reserved)
{
	WriteToLogFile("RegDeleteKeyEx"); return rRegDeleteKeyEx(hKey, lpSubKey, samDesired, Reserved);
}

LONG WINAPI cRegDeleteValue(HKEY hKey, LPCTSTR lpValueName)
{
	WriteToLogFile("RegDeleteValue"); return rRegDeleteValue(hKey, lpValueName);
}

LONG WINAPI cRegGetValue(HKEY hKey, LPCTSTR lpSubKey, LPCTSTR lpValue, DWORD dwFlags, LPDWORD pdwType, PVOID pvData, LPDWORD pcbData)
{
	WriteToLogFile("RegGetValue"); return rRegGetValue(hKey, lpSubKey, lpValue, dwFlags, pdwType, pvData, pcbData);
}

LONG WINAPI cRegLoadAppKey(LPCTSTR lpFile, PHKEY phkResult, REGSAM samDesired, DWORD dwOptions, DWORD Reserved)
{
	WriteToLogFile("GetLoadAppKey"); return rRegLoadAppKey(lpFile, phkResult, samDesired, dwOptions, Reserved);
}

LONG WINAPI cRegLoadKey(HKEY hKey, LPCTSTR lpSubKey,LPCTSTR lpFile)
{
	WriteToLogFile("RegLoadKey"); return rRegLoadKey(hKey, lpSubKey, lpFile);
}

LONG WINAPI cRegOpenKeyEx(HKEY hKey, LPCTSTR lpSubKey, DWORD ulOptions, REGSAM samDesired, PHKEY phkResult)
{
	WriteToLogFile("RegOpenKeyEx"); return rRegOpenKeyEx(hKey, lpSubKey, ulOptions, samDesired, phkResult);
}

LONG WINAPI cRegReplaceKey(HKEY hKey, LPCTSTR lpSubKey, LPCTSTR lpNewFile, LPCTSTR lpOldFile)
{
	WriteToLogFile("GetReplaceKey"); return rRegReplaceKey(hKey, lpSubKey, lpNewFile, lpOldFile);
}

LONG WINAPI cRegRestoreKey(HKEY hKey, LPCTSTR lpFile, DWORD dwFlags)
{
	WriteToLogFile("RegRestoreKey"); return rRegRestoreKey(hKey, lpFile, dwFlags);
}

LONG WINAPI cRegSaveKey(HKEY hKey, LPCTSTR lpFile, LPSECURITY_ATTRIBUTES lpSecurityAttributes)
{
	WriteToLogFile("RegSaveKey"); return rRegSaveKey(hKey, lpFile, lpSecurityAttributes);
}

LONG WINAPI cRegSaveKeyEx(HKEY hKey, LPCTSTR lpFile, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD Flags)
{
	WriteToLogFile("RegSaveKeyEx"); return rRegSaveKeyEx(hKey, lpFile, lpSecurityAttributes, Flags);
}

LONG WINAPI cRegSetKeyValue(HKEY hKey, LPCTSTR lpSubKey, LPCTSTR lpValueName, DWORD dwType, LPCVOID lpData, DWORD cbData)
{
	WriteToLogFile("RegSetKeyValue"); return rRegSetKeyValue(hKey, lpSubKey, lpValueName, dwType, lpData, cbData);
}

LONG WINAPI cRegSetValueEx(HKEY hKey, LPCTSTR lpValueName, DWORD Reserved, DWORD dwType, const BYTE *lpData, DWORD cbData)
{
	WriteToLogFile("RegSetValueEx"); return rRegSetValueEx(hKey, lpValueName, Reserved, dwType, lpData, cbData);
}

LONG WINAPI cRegUnLoadKey(HKEY hKey, LPCTSTR lpSubKey)
{
	WriteToLogFile("RegUnLoadKey"); return rRegUnLoadKey(hKey, lpSubKey);
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		logfile.open("C:\\logs\\log.txt");
		if(!logfile.is_open())
			return -1;
		WriteToLogFile("DLL_PROCESS_ATTACH");
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		//---------file management functions start----------//
		DetourAttach(&(PVOID&)rCancelIo, cCancelIo);
		DetourAttach(&(PVOID&)rCancelIoEx, cCancelIoEx);
		DetourAttach(&(PVOID&)rCancelSynchronousIo, cCancelSynchronousIo);
		DetourAttach(&(PVOID&)rCreateIoCompletionPort, cCreateIoCompletionPort);
		DetourAttach(&(PVOID&)rFlushFileBuffers, cFlushFileBuffers);
		DetourAttach(&(PVOID&)rGetQueuedCompletionStatus, cGetQueuedCompletionStatus);
		DetourAttach(&(PVOID&)rGetQueuedCompletionStatusEx, cGetQueuedCompletionStatusEx);
		DetourAttach(&(PVOID&)rLockFile, cLockFile);
		DetourAttach(&(PVOID&)rLockFileEx, cLockFileEx);
		DetourAttach(&(PVOID&)rPostQueuedCompletionStatus, cPostQueuedCompletionStatus);
		DetourAttach(&(PVOID&)rReadFile, cReadFile);
		DetourAttach(&(PVOID&)rReadFileEx, cReadFileEx);
		DetourAttach(&(PVOID&)rReadFileScatter, cReadFileScatter);
		DetourAttach(&(PVOID&)rSetEndOfFile, cSetEndOfFile);
		DetourAttach(&(PVOID&)rSetFileCompletionNotificationModes, cSetFileCompletionNotificationModes);
		DetourAttach(&(PVOID&)rSetFileIoOverlappedRange, cSetFileIoOverlappedRange);
		DetourAttach(&(PVOID&)rSetFilePointer, cSetFilePointer);
		DetourAttach(&(PVOID&)rSetFilePointerEx, cSetFilePointerEx);
		DetourAttach(&(PVOID&)rUnlockFile, cUnlockFile);
		DetourAttach(&(PVOID&)rUnlockFileEx, cUnlockFileEx);
		DetourAttach(&(PVOID&)rWriteFile, cWriteFile);
		DetourAttach(&(PVOID&)rWriteFileEx, cWriteFileEx);
		DetourAttach(&(PVOID&)rWriteFileGather, cWriteFileGather);
		DetourAttach(&(PVOID&)rAddUsersToEncryptedFile, cAddUsersToEncryptedFile);
		DetourAttach(&(PVOID&)rCloseEncryptedFileRaw, cCloseEncryptedFileRaw);
		DetourAttach(&(PVOID&)rDecryptFile, cDecryptFile);
		DetourAttach(&(PVOID&)rDuplicateEncryptionInfoFile, cDuplicateEncryptionInfoFile);
		DetourAttach(&(PVOID&)rEncryptFile, cEncryptFile);
		DetourAttach(&(PVOID&)rEncryptionDisable, cEncryptionDisable);
		DetourAttach(&(PVOID&)rFileEncryptionStatus, cFileEncryptionStatus);
		DetourAttach(&(PVOID&)rFreeEncryptionCertificateHashList, cFreeEncryptionCertificateHashList);
		DetourAttach(&(PVOID&)rOpenEncryptedFileRaw, cOpenEncryptedFileRaw);
		DetourAttach(&(PVOID&)rQueryRecoveryAgentsOnEncryptedFile, cQueryRecoveryAgentsOnEncryptedFile);
		DetourAttach(&(PVOID&)rQueryUsersOnEncryptedFile, cQueryUsersOnEncryptedFile);
		DetourAttach(&(PVOID&)rReadEncryptedFileRaw, cReadEncryptedFileRaw);
		DetourAttach(&(PVOID&)rRemoveUsersFromEncryptedFile, cRemoveUsersFromEncryptedFile);
		DetourAttach(&(PVOID&)rWriteEncryptedFileRaw, cWriteEncryptedFileRaw);
		DetourAttach(&(PVOID&)rSetUserFileEncryptionKey, cSetUserFileEncryptionKey);
		DetourAttach(&(PVOID&)rAreFileApisANSI, cAreFileApisANSI);
		DetourAttach(&(PVOID&)rCheckNameLegalDOS8Dot3, cCheckNameLegalDOS8Dot3);
		DetourAttach(&(PVOID&)rCopyFile, cCopyFile);
		DetourAttach(&(PVOID&)rCopyFileEx, cCopyFileEx);
		DetourAttach(&(PVOID&)rCopyFileTransacted, cCopyFileTransacted);
		DetourAttach(&(PVOID&)rCreateFile, cCreateFile);
		DetourAttach(&(PVOID&)rCreateFileTransacted, cCreateFileTransacted);
		DetourAttach(&(PVOID&)rCreateHardLink, cCreateHardLink);
		DetourAttach(&(PVOID&)rCreateHardLinkTransacted, cCreateHardLinkTransacted);
		DetourAttach(&(PVOID&)rCreateSymbolicLink, cCreateSymbolicLink);
		DetourAttach(&(PVOID&)rCreateSymbolicLinkTransacted, cCreateSymbolicLinkTransacted);
		DetourAttach(&(PVOID&)rDeleteFile, cDeleteFile);
		DetourAttach(&(PVOID&)rDeleteFileTransacted, cDeleteFileTransacted);
		DetourAttach(&(PVOID&)rFindClose, cFindClose);
		DetourAttach(&(PVOID&)rFindFirstFile, cFindFirstFile);
		DetourAttach(&(PVOID&)rFindFirstFileEx, cFindFirstFileEx);
		DetourAttach(&(PVOID&)rFindFirstFileNameTransactedW, cFindFirstFileNameTransactedW);
		DetourAttach(&(PVOID&)rFindFirstFileNameW, cFindFirstFileNameW);
		DetourAttach(&(PVOID&)rFindFirstFileTransacted, cFindFirstFileTransacted);
		DetourAttach(&(PVOID&)rFindFirstStreamTransactedW, cFindFirstStreamTransactedW);
		DetourAttach(&(PVOID&)rFindFirstStreamW, cFindFirstStreamW);
		DetourAttach(&(PVOID&)rFindNextFile, cFindNextFile);
		DetourAttach(&(PVOID&)rFindNextFileNameW, cFindNextFileNameW);
		DetourAttach(&(PVOID&)rFindNextStreamW, cFindNextStreamW);
		DetourAttach(&(PVOID&)rGetBinaryType, cGetBinaryType);
		DetourAttach(&(PVOID&)rGetCompressedFileSize, cGetCompressedFileSize);
		DetourAttach(&(PVOID&)rGetCompressedFileSizeTransacted, cGetCompressedFileSizeTransacted);
		DetourAttach(&(PVOID&)rGetFileAttributes, cGetFileAttributes);
		DetourAttach(&(PVOID&)rGetFileAttributesEx, cGetFileAttributesEx);
		DetourAttach(&(PVOID&)rGetFileAttributesTransacted, cGetFileAttributesTransacted);
		DetourAttach(&(PVOID&)rGetFileBandwidthReservation, cGetFileBandwidthReservation);
		DetourAttach(&(PVOID&)rGetFileInformationByHandle, cGetFileInformationByHandle);
		DetourAttach(&(PVOID&)rGetFileInformationByHandleEx, cGetFileInformationByHandleEx);
		DetourAttach(&(PVOID&)rGetFileSize, cGetFileSize);
		DetourAttach(&(PVOID&)rGetFileSizeEx, cGetFileSizeEx);
		DetourAttach(&(PVOID&)rGetFileType, cGetFileType);
		DetourAttach(&(PVOID&)rGetFinalPathNameByHandle, cGetFinalPathNameByHandle);
		DetourAttach(&(PVOID&)rGetFullPathName, cGetFullPathName);
		DetourAttach(&(PVOID&)rGetFullPathNameTransacted, cGetFullPathNameTransacted);
		DetourAttach(&(PVOID&)rGetLongPathName, cGetLongPathName);
		DetourAttach(&(PVOID&)rGetLongPathNameTransacted, cGetLongPathNameTransacted);
		DetourAttach(&(PVOID&)rGetShortPathName, cGetShortPathName);
		DetourAttach(&(PVOID&)rGetTempFileName, cGetTempFileName);
		DetourAttach(&(PVOID&)rGetTempPath, cGetTempPath);
		DetourAttach(&(PVOID&)rMoveFile, cMoveFile);
		DetourAttach(&(PVOID&)rMoveFileEx, cMoveFileEx);
		DetourAttach(&(PVOID&)rMoveFileTransacted, cMoveFileTransacted);
		DetourAttach(&(PVOID&)rMoveFileWithProgress, cMoveFileWithProgress);
		DetourAttach(&(PVOID&)rOpenFile, cOpenFile);
		DetourAttach(&(PVOID&)rOpenFileById, cOpenFileById);
		DetourAttach(&(PVOID&)rReOpenFile, cReOpenFile);
		DetourAttach(&(PVOID&)rReplaceFile, cReplaceFile);
		DetourAttach(&(PVOID&)rSearchPath, cSearchPath);
		DetourAttach(&(PVOID&)rSetFileApisToANSI, cSetFileApisToANSI);
		DetourAttach(&(PVOID&)rSetFileApisToOEM, cSetFileApisToOEM);
		DetourAttach(&(PVOID&)rSetFileAttributes, cSetFileAttributes);
		DetourAttach(&(PVOID&)rSetFileAttributesTransacted, cSetFileAttributesTransacted);
		DetourAttach(&(PVOID&)rSetFileBandwidthReservation, cSetFileBandwidthReservation);
		DetourAttach(&(PVOID&)rSetFileInformationByHandle, cSetFileInformationByHandle);
		DetourAttach(&(PVOID&)rSetFileShortName, cSetFileShortName);
		DetourAttach(&(PVOID&)rSetFileValidData, cSetFileValidData);
		DetourAttach(&(PVOID&)rSetSearchPathMode, cSetSearchPathMode);
		//---------file management functions end----------//

		//---------registry functions start----------//
		DetourAttach(&(PVOID&)rRegCloseKey, cRegCloseKey);
		DetourAttach(&(PVOID&)rRegCreateKeyEx, cRegCreateKeyEx);
		DetourAttach(&(PVOID&)rRegConnectRegistry, cRegConnectRegistry);
		DetourAttach(&(PVOID&)rRegDeleteKey, cRegDeleteKey);
		DetourAttach(&(PVOID&)rRegDeleteKeyEx, cRegDeleteKeyEx);
		DetourAttach(&(PVOID&)rRegDeleteValue, cRegDeleteValue);
		DetourAttach(&(PVOID&)rRegGetValue, cRegGetValue);
		DetourAttach(&(PVOID&)rRegLoadAppKey, cRegLoadAppKey);
		DetourAttach(&(PVOID&)rRegLoadKey, cRegLoadKey);
		DetourAttach(&(PVOID&)rRegOpenKeyEx, cRegOpenKeyEx);
		DetourAttach(&(PVOID&)rRegReplaceKey, cRegReplaceKey);
		DetourAttach(&(PVOID&)rRegRestoreKey, cRegRestoreKey); 
		DetourAttach(&(PVOID&)rRegSaveKey, cRegSaveKey);
		DetourAttach(&(PVOID&)rRegSaveKeyEx, cRegSaveKeyEx);
		DetourAttach(&(PVOID&)rRegSetKeyValue, cRegSetKeyValue);
		DetourAttach(&(PVOID&)rRegSetValueEx, cRegSetValueEx);
		DetourAttach(&(PVOID&)rRegUnLoadKey, cRegUnLoadKey);
		//---------registry functions end----------//

		if(DetourTransactionCommit() == NO_ERROR)
			WriteToLogFile("Transaction commit success");
		else
			WriteToLogFile("Transaction commit failure");
		break;
	case DLL_PROCESS_DETACH:
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());

		//---------file management functions start----------//
		DetourDetach(&(PVOID&)rCancelIo, cCancelIo);
		DetourDetach(&(PVOID&)rCancelIoEx, cCancelIoEx);
		DetourDetach(&(PVOID&)rCancelSynchronousIo, cCancelSynchronousIo);
		DetourDetach(&(PVOID&)rCreateIoCompletionPort, cCreateIoCompletionPort);
		DetourDetach(&(PVOID&)rFlushFileBuffers, cFlushFileBuffers);
		DetourDetach(&(PVOID&)rGetQueuedCompletionStatus, cGetQueuedCompletionStatus);
		DetourDetach(&(PVOID&)rGetQueuedCompletionStatusEx, cGetQueuedCompletionStatusEx);
		DetourDetach(&(PVOID&)rLockFile, cLockFile);
		DetourDetach(&(PVOID&)rLockFileEx, cLockFileEx);
		DetourDetach(&(PVOID&)rPostQueuedCompletionStatus, cPostQueuedCompletionStatus);
		DetourDetach(&(PVOID&)rReadFile, cReadFile);
		DetourDetach(&(PVOID&)rReadFileEx, cReadFileEx);
		DetourDetach(&(PVOID&)rReadFileScatter, cReadFileScatter);
		DetourDetach(&(PVOID&)rSetEndOfFile, cSetEndOfFile);
		DetourDetach(&(PVOID&)rSetFileCompletionNotificationModes, cSetFileCompletionNotificationModes);
		DetourDetach(&(PVOID&)rSetFileIoOverlappedRange, cSetFileIoOverlappedRange);
		DetourDetach(&(PVOID&)rSetFilePointer, cSetFilePointer);
		DetourDetach(&(PVOID&)rSetFilePointerEx, cSetFilePointerEx);
		DetourDetach(&(PVOID&)rUnlockFile, cUnlockFile);
		DetourDetach(&(PVOID&)rUnlockFileEx, cUnlockFileEx);
		DetourDetach(&(PVOID&)rWriteFile, cWriteFile);
		DetourDetach(&(PVOID&)rWriteFileEx, cWriteFileEx);
		DetourDetach(&(PVOID&)rWriteFileGather, cWriteFileGather);
		DetourDetach(&(PVOID&)rAddUsersToEncryptedFile, cAddUsersToEncryptedFile);
		DetourDetach(&(PVOID&)rCloseEncryptedFileRaw, cCloseEncryptedFileRaw);
		DetourDetach(&(PVOID&)rDecryptFile, cDecryptFile);
		DetourDetach(&(PVOID&)rDuplicateEncryptionInfoFile, cDuplicateEncryptionInfoFile);
		DetourDetach(&(PVOID&)rEncryptFile, cEncryptFile);
		DetourDetach(&(PVOID&)rEncryptionDisable, cEncryptionDisable);
		DetourDetach(&(PVOID&)rFileEncryptionStatus, cFileEncryptionStatus);
		DetourDetach(&(PVOID&)rFreeEncryptionCertificateHashList, cFreeEncryptionCertificateHashList);
		DetourDetach(&(PVOID&)rOpenEncryptedFileRaw, cOpenEncryptedFileRaw);
		DetourDetach(&(PVOID&)rQueryRecoveryAgentsOnEncryptedFile, cQueryRecoveryAgentsOnEncryptedFile);
		DetourDetach(&(PVOID&)rQueryUsersOnEncryptedFile, cQueryUsersOnEncryptedFile);
		DetourDetach(&(PVOID&)rReadEncryptedFileRaw, cReadEncryptedFileRaw);
		DetourDetach(&(PVOID&)rRemoveUsersFromEncryptedFile, cRemoveUsersFromEncryptedFile);
		DetourDetach(&(PVOID&)rWriteEncryptedFileRaw, cWriteEncryptedFileRaw);
		DetourDetach(&(PVOID&)rSetUserFileEncryptionKey, cSetUserFileEncryptionKey);
		DetourDetach(&(PVOID&)rAreFileApisANSI, cAreFileApisANSI);
		DetourDetach(&(PVOID&)rCheckNameLegalDOS8Dot3, cCheckNameLegalDOS8Dot3);
		DetourDetach(&(PVOID&)rCopyFile, cCopyFile);
		DetourDetach(&(PVOID&)rCopyFileEx, cCopyFileEx);
		DetourDetach(&(PVOID&)rCopyFileTransacted, cCopyFileTransacted);
		DetourDetach(&(PVOID&)rCreateFile, cCreateFile);
		DetourDetach(&(PVOID&)rCreateFileTransacted, cCreateFileTransacted);
		DetourDetach(&(PVOID&)rCreateHardLink, cCreateHardLink);
		DetourDetach(&(PVOID&)rCreateHardLinkTransacted, cCreateHardLinkTransacted);
		DetourDetach(&(PVOID&)rCreateSymbolicLink, cCreateSymbolicLink);
		DetourDetach(&(PVOID&)rCreateSymbolicLinkTransacted, cCreateSymbolicLinkTransacted);
		DetourDetach(&(PVOID&)rDeleteFile, cDeleteFile);
		DetourDetach(&(PVOID&)rDeleteFileTransacted, cDeleteFileTransacted);
		DetourDetach(&(PVOID&)rFindClose, cFindClose);
		DetourDetach(&(PVOID&)rFindFirstFile, cFindFirstFile);
		DetourDetach(&(PVOID&)rFindFirstFileEx, cFindFirstFileEx);
		DetourDetach(&(PVOID&)rFindFirstFileNameTransactedW, cFindFirstFileNameTransactedW);
		DetourDetach(&(PVOID&)rFindFirstFileNameW, cFindFirstFileNameW);
		DetourDetach(&(PVOID&)rFindFirstFileTransacted, cFindFirstFileTransacted);
		DetourDetach(&(PVOID&)rFindFirstStreamTransactedW, cFindFirstStreamTransactedW);
		DetourDetach(&(PVOID&)rFindFirstStreamW, cFindFirstStreamW);
		DetourDetach(&(PVOID&)rFindNextFile, cFindNextFile);
		DetourDetach(&(PVOID&)rFindNextFileNameW, cFindNextFileNameW);
		DetourDetach(&(PVOID&)rFindNextStreamW, cFindNextStreamW);
		DetourDetach(&(PVOID&)rGetBinaryType, cGetBinaryType);
		DetourDetach(&(PVOID&)rGetCompressedFileSize, cGetCompressedFileSize);
		DetourDetach(&(PVOID&)rGetCompressedFileSizeTransacted, cGetCompressedFileSizeTransacted);
		DetourDetach(&(PVOID&)rGetFileAttributes, cGetFileAttributes);
		DetourDetach(&(PVOID&)rGetFileAttributesEx, cGetFileAttributesEx);
		DetourDetach(&(PVOID&)rGetFileAttributesTransacted, cGetFileAttributesTransacted);
		DetourDetach(&(PVOID&)rGetFileBandwidthReservation, cGetFileBandwidthReservation);
		DetourDetach(&(PVOID&)rGetFileInformationByHandle, cGetFileInformationByHandle);
		DetourDetach(&(PVOID&)rGetFileInformationByHandleEx, cGetFileInformationByHandleEx);
		DetourDetach(&(PVOID&)rGetFileSize, cGetFileSize);
		DetourDetach(&(PVOID&)rGetFileSizeEx, cGetFileSizeEx);
		DetourDetach(&(PVOID&)rGetFileType, cGetFileType);
		DetourDetach(&(PVOID&)rGetFinalPathNameByHandle, cGetFinalPathNameByHandle);
		DetourDetach(&(PVOID&)rGetFullPathName, cGetFullPathName);
		DetourDetach(&(PVOID&)rGetFullPathNameTransacted, cGetFullPathNameTransacted);
		DetourDetach(&(PVOID&)rGetLongPathName, cGetLongPathName);
		DetourDetach(&(PVOID&)rGetLongPathNameTransacted, cGetLongPathNameTransacted);
		DetourDetach(&(PVOID&)rGetShortPathName, cGetShortPathName);
		DetourDetach(&(PVOID&)rGetTempFileName, cGetTempFileName);
		DetourDetach(&(PVOID&)rGetTempPath, cGetTempPath);
		DetourDetach(&(PVOID&)rMoveFile, cMoveFile);
		DetourDetach(&(PVOID&)rMoveFileEx, cMoveFileEx);
		DetourDetach(&(PVOID&)rMoveFileTransacted, cMoveFileTransacted);
		DetourDetach(&(PVOID&)rMoveFileWithProgress, cMoveFileWithProgress);
		DetourDetach(&(PVOID&)rOpenFile, cOpenFile);
		DetourDetach(&(PVOID&)rOpenFileById, cOpenFileById);
		DetourDetach(&(PVOID&)rReOpenFile, cReOpenFile);
		DetourDetach(&(PVOID&)rReplaceFile, cReplaceFile);
		DetourDetach(&(PVOID&)rSearchPath, cSearchPath);
		DetourDetach(&(PVOID&)rSetFileApisToANSI, cSetFileApisToANSI);
		DetourDetach(&(PVOID&)rSetFileApisToOEM, cSetFileApisToOEM);
		DetourDetach(&(PVOID&)rSetFileAttributes, cSetFileAttributes);
		DetourDetach(&(PVOID&)rSetFileAttributesTransacted, cSetFileAttributesTransacted);
		DetourDetach(&(PVOID&)rSetFileBandwidthReservation, cSetFileBandwidthReservation);
		DetourDetach(&(PVOID&)rSetFileInformationByHandle, cSetFileInformationByHandle);
		DetourDetach(&(PVOID&)rSetFileShortName, cSetFileShortName);
		DetourDetach(&(PVOID&)rSetFileValidData, cSetFileValidData);
		DetourDetach(&(PVOID&)rSetSearchPathMode, cSetSearchPathMode);
		//---------file management functions end----------//

		//---------registry functions start----------//
		DetourDetach(&(PVOID&)rRegCloseKey, cRegCloseKey);
		DetourDetach(&(PVOID&)rRegCreateKeyEx, cRegCreateKeyEx);
		DetourDetach(&(PVOID&)rRegConnectRegistry, cRegConnectRegistry);
		DetourDetach(&(PVOID&)rRegDeleteKey, cRegDeleteKey);
		DetourDetach(&(PVOID&)rRegDeleteKeyEx, cRegDeleteKeyEx);
		DetourDetach(&(PVOID&)rRegDeleteValue, cRegDeleteValue);
		DetourDetach(&(PVOID&)rRegGetValue, cRegGetValue);
		DetourDetach(&(PVOID&)rRegLoadAppKey, cRegLoadAppKey);
		DetourDetach(&(PVOID&)rRegLoadKey, cRegLoadKey);
		DetourDetach(&(PVOID&)rRegOpenKeyEx, cRegOpenKeyEx);
		DetourDetach(&(PVOID&)rRegReplaceKey, cRegReplaceKey);
		DetourDetach(&(PVOID&)rRegRestoreKey, cRegRestoreKey); 
		DetourDetach(&(PVOID&)rRegSaveKey, cRegSaveKey);
		DetourDetach(&(PVOID&)rRegSaveKeyEx, cRegSaveKeyEx);
		DetourDetach(&(PVOID&)rRegSetKeyValue, cRegSetKeyValue);
		DetourDetach(&(PVOID&)rRegSetValueEx, cRegSetValueEx);
		DetourDetach(&(PVOID&)rRegUnLoadKey, cRegUnLoadKey);
		//---------registry functions end----------//

		if(DetourTransactionCommit() == NO_ERROR)
			WriteToLogFile("Transaction commit success");
		else
			WriteToLogFile("Transaction commit failure");
		WriteToLogFile("DLL_PROCESS_DETACH");
		EndLogging();
		break;
	}
	return TRUE;
}

