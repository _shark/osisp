//#include <ntifs.h>
#include <ntddk.h>

NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath);
VOID UnloadRoutine(IN PDRIVER_OBJECT DriverObject);
VOID CreateProcCallback(PEPROCESS Process, HANDLE ProcessId, PPS_CREATE_NOTIFY_INFO CreateInfo);

#pragma alloc_text(INIT, DriverEntry)
#pragma alloc_text(PAGE, UnloadRoutine)

static HANDLE hProcHandle;

NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath)
{
	NTSTATUS status = STATUS_SUCCESS;

	DriverObject->DriverUnload = UnloadRoutine;
	status = PsSetCreateProcessNotifyRoutineEx(CreateProcCallback, FALSE);
	if (!NT_SUCCESS(status)) 
		DbgPrint("PsSetCreateProcessNotifyRoutine error");
	else
		DbgPrint("DriverEntry");

	return status;
}

VOID UnloadRoutine(IN PDRIVER_OBJECT DriverObject)
{
	NTSTATUS status = STATUS_SUCCESS;

	status = PsSetCreateProcessNotifyRoutineEx(CreateProcCallback, TRUE);
	if (!NT_SUCCESS(status)) 
		DbgPrint("PsSetCreateProcessNotifyRoutine error");
	DbgPrint("Unload Routine");
}

VOID CreateProcCallback(PEPROCESS process, HANDLE ProcessId, PPS_CREATE_NOTIFY_INFO CreateInfo)
{
	if(CreateInfo != NULL) // process is creating
	{
		char proc_name[256];
		ANSI_STRING ansi_string;

		memset(proc_name, 0, 256);
		RtlUnicodeStringToAnsiString(&ansi_string, CreateInfo->ImageFileName, TRUE);
		memcpy(proc_name, ansi_string.Buffer, ansi_string.Length);
		DbgPrint("Process %s is statring", proc_name);
		RtlFreeAnsiString(&ansi_string);
		hProcHandle = ProcessId;
	}
	else //process is exiting
	{
		if(hProcHandle == ProcessId)
		{
			DbgPrint("Process calc.exe is terminating");
		}
	}
	//if(TRUE)//Create)
	//{
	//	ULONG ProcNameRequiredLength;
	//	NTSTATUS status;
	//	UNICODE_STRING ProcName;
	//	WCHAR EmptyString[256];

	//	memset(EmptyString, 0, wcslen(EmptyString)); 
	//	RtlInitUnicodeString(&ProcName, EmptyString);


	//	
	//}
	//else
	//{
	//}
}